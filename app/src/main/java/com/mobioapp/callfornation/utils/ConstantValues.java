package com.mobioapp.callfornation.utils;

import android.content.Context;

import com.mobioapp.callfornation.R;

public class ConstantValues {

    public static String getString(Context context, int id) {
        return context.getResources().getString(id);
    }

    public static long systemCurrentTime = 0;
    public static final String LOGIN_TAG = "LOGIN_TAG";
    public static final String LANG_SHORT_CODE_TAG = "LANG_SHORT_CODE_TAG";
    public static final String LANG_TAG = "LANG_TAG";
    public static final String IS_FIRST_TIME_APP = "IS_FIRST_TIME_APP";
    public static final String USER_LOGIN_TAG = "USER_LOGIN_TAG";

    public static final int TAB_TITLE_TOTAL_GOVT_ORG = R.string.govt_org;
    public static final int TAB_TITLE_TOTAL_PRIVATE_ORG = R.string.private_org;
    public static final int TAB_TITLE_TOTAL_INDIVIDUAL = R.string.individual;

    public class REST {
        public static final String CONTENT_TYPE = "Content-Type";
        public static final String TYPE_JSON = "application/json";

        public class REQUEST {
            public static final String TAG_USER_ID = "userID";
            public static final String PROJECT_ID = "projectId";
        }
    }

    public static final String RELIEF_REQUEST_FOR_INDIVIDUAL = "1";
    public static final String RELIEF_REQUEST_FOR_OTHERS = "2";
    public static final String RELIEF_REQUEST_FOR_ALL = "0";

    public static final String RELIEF_DISTRIBUTION_PENDING = "1";
    public static final String RELIEF_DISTRIBUTION_NEW = "4";
    public static final String RELIEF_DISTRIBUTION_DONE = "2";
    public static final String RELIEF_DISTRIBUTION_CANCEL = "3";
    public static final String RELIEF_DISTRIBUTION_ALL = "0";

    public static final String TAG_WHICH_OPTION_SELECTED = "whichOption";
    public static final String TAG_SELECTED_RELIEF_DISTRIBUTION = "selected_relief_dist";
}
