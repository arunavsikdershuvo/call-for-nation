package com.mobioapp.callfornation.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mobioapp.callfornation.R;

public class MyCustomToast extends Toast {
    private Context context;

    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */
    public MyCustomToast(Context context) {
        super(context);
        this.context = context;
    }

    public void showToast(String value) {
        if (ConstantValues.systemCurrentTime == 0) {
            ConstantValues.systemCurrentTime = System.currentTimeMillis();
        } else {
            if (System.currentTimeMillis() - ConstantValues.systemCurrentTime > 2000) {
                ConstantValues.systemCurrentTime = 0;
            } else {
                return;
            }
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_toast_layout, null);

        TextView text = layout.findViewById(R.id.txtview_toast);
        text.setText(value);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
}
