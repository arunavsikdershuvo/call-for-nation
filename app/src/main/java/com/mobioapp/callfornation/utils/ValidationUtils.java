package com.mobioapp.callfornation.utils;

import android.app.Activity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

public class ValidationUtils {

    public ValidationUtils(Activity activity) {
    }

    public boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[\\@#$%^&+=!*+'?:,.()}{_//~-])(?=\\S+$).{4,}$";
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[\\[\\]@#$%^&+=!*'?:,.()}{_~-])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean isValid(String pass) {
        if (pass != null && pass.trim().length() > 0) {
            return true;
        }
        return false;
    }

    public boolean isPhoneNumberValid(String countryCode, String editTextPhoneNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.createInstance(CFNApplication.getAppContext());
        Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber().setCountryCode(Integer.parseInt(countryCode)).setNationalNumber(Long.parseLong(editTextPhoneNumber));
        if (phoneNumberUtil.isValidNumber(phoneNumber)) {
            return true;
        }
        return false;
    }

}
