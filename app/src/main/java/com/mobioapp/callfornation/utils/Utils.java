package com.mobioapp.callfornation.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mobioapp.callfornation.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_CANCEL;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_DONE;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_PENDING;

public class Utils {

    public static boolean isOnline(Context mContext) {
        NetworkInfo netInfo = null;
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            netInfo = connectivityManager.getActiveNetworkInfo();
            return (netInfo != null && netInfo.isConnected());
        }
        return false;
    }

    public static String getCurrentDateToDisplay() {
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy");
        return df.format(c);
    }

    public static String getCurrentDateForServer() {
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return df.format(c);
    }

    public static String convertDateToHumanReadableFromServer(String date) {
        DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        Date d = null;
        try {
            d = f1.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        String x = f2.format(d);

        return x;
    }

    public static void redirectToAppSettings(Context mContext) {

            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
            intent.setData(uri);
            mContext.startActivity(intent);


    }


    public static String getStatusNameForDistribution(String status) {

        if (status == null || status.isEmpty())
            return "Not Found";

        if (RELIEF_DISTRIBUTION_PENDING.equals(status))
            return "Pending";
        if (RELIEF_DISTRIBUTION_DONE.equals(status))
            return "Distributed";
        if (RELIEF_DISTRIBUTION_CANCEL.equals(status))
            return "Cancelled";

        return "Error!";
    }

    public static int getTextColorForDistributionStatus(Context context, String status) {

        if (status == null || status.isEmpty())
            return ContextCompat.getColor(context, R.color.colorRed);

        if (RELIEF_DISTRIBUTION_PENDING.equals(status))
            return ContextCompat.getColor(context, R.color.button_color);
        if (RELIEF_DISTRIBUTION_DONE.equals(status))
            return ContextCompat.getColor(context, R.color.bar_color);
        if (RELIEF_DISTRIBUTION_CANCEL.equals(status))
            return ContextCompat.getColor(context, R.color.colorRedLight);

        return ContextCompat.getColor(context, R.color.colorRed);
    }

    public static class DividerItemDecorator extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public DividerItemDecorator(Drawable divider) {
            mDivider = divider;
        }

        @Override
        public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
            int dividerLeft = parent.getPaddingLeft();
            int dividerRight = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i <= childCount - 2; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int dividerTop = child.getBottom() + params.bottomMargin;
                int dividerBottom = dividerTop + mDivider.getIntrinsicHeight();

                mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
                mDivider.draw(canvas);
            }
        }
    }
}
