package com.mobioapp.callfornation.utils;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;


public class CFNApplication extends Application {

    public static final String TAG = CFNApplication.class.getSimpleName();
    private static Context context;
    private static CFNApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = getApplicationContext();
    }

    public static Context getAppContext() {
        return context;
    }

    public static Resources getAppResources() {
        return getAppContext().getResources();
    }

    public static synchronized CFNApplication getInstance() {
        return mInstance;
    }


}
