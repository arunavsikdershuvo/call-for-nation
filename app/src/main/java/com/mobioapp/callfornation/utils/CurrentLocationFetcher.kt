package com.mobioapp.callfornation.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CurrentLocationFetcher(val context: Activity) {

    val PERMISSION_REQUEST_CODE = 215

    private val locationService by lazy {
        context.getSystemService(Context.LOCATION_SERVICE) as
                LocationManager
    }

    val locationLiveData = MutableLiveData<LatLng>()
    val currentAddressLiveData = MutableLiveData<String>()

    fun requestPermission() {
        ActivityCompat.requestPermissions(context,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ),
                PERMISSION_REQUEST_CODE
        )
    }

    fun fetchOnce(): Boolean {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return false
        }
        locationService.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, object :
                LocationListener {
            fun updateLocation(loc: Location) {
                val latLng = LatLng(loc.latitude, loc.longitude)
                locationLiveData.postValue(latLng)
                GlobalScope.launch {
                    currentAddressLiveData.postValue(fetchAreaInfo(latLng))
                }
            }

            init {
                GlobalScope.launch {
                    locationService.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)?.apply {
                        updateLocation(this)
                    }
                }
            }

            override fun onLocationChanged(location: Location?) {
                locationService.removeUpdates(this)
                location?.apply {
                    updateLocation(this)
                }
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

            }

            override fun onProviderEnabled(provider: String?) {

            }

            override fun onProviderDisabled(provider: String?) {

            }

        })

        return true
    }


    fun fetchAreaInfo(latLng: LatLng): String? {
        val geocoder = Geocoder(context)
        val locationData = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)[0]
        return locationData.getAddressLine(0)
    }
}