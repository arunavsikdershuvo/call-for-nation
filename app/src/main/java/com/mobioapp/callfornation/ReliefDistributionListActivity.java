package com.mobioapp.callfornation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mobioapp.callfornation.adapter.ReliefDistributionListAdapter;
import com.mobioapp.callfornation.controller.ReliefDistributionListController;
import com.mobioapp.callfornation.database.PreferenceUtil;
import com.mobioapp.callfornation.listener.ReliefDistributionListListener;
import com.mobioapp.callfornation.model.ReliefDistribution;
import com.mobioapp.callfornation.model.ReliefDistributionListInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;
import com.mobioapp.callfornation.utils.Utils;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_CANCEL;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_DONE;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_NEW;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_PENDING;
import static com.mobioapp.callfornation.utils.ConstantValues.TAG_SELECTED_RELIEF_DISTRIBUTION;
import static com.mobioapp.callfornation.utils.ConstantValues.TAG_WHICH_OPTION_SELECTED;
import static com.mobioapp.callfornation.utils.Utils.isOnline;

public class ReliefDistributionListActivity extends AppCompatActivity implements ReliefDistributionListListener, ReliefDistributionListAdapter.OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.linear_no_content)
    LinearLayout linearLayoutNoContent;

    @BindView(R.id.floatingActionButton)
    FloatingActionButton floatingActionButton;

    private Activity activity;
    private PreferenceUtil preferenceUtil;

    private ReliefDistributionListAdapter adapter;
    private List<ReliefDistribution> reliefDistributionList = null;
    private ReliefDistributionListController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relief_distribution_list);
        ButterKnife.bind(this);

        setToolbar();
        initViews();
//        loadData(); //code is in onResume

    }

    private void loadData() {

        progressBar.setVisibility(View.VISIBLE);
        controller = new ReliefDistributionListController(this);
        controller.start(preferenceUtil.retrieveLoginData().getUser_id());

    }

    private void initViews() {

        activity = ReliefDistributionListActivity.this;
        preferenceUtil = new PreferenceUtil(this);

        if (reliefDistributionList == null)
            reliefDistributionList = new ArrayList<>();

        adapter = new ReliefDistributionListAdapter(activity, this, reliefDistributionList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void setToolbar() {

        if (toolbar != null)
            toolbar.setTitle(getResources().getString(R.string.relief_distribution));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void distributionInfoCompleted(ReliefDistributionListInfo reliefDistributionListInfo) {
        progressBar.setVisibility(View.GONE);
        if (reliefDistributionListInfo != null && "201".equals(reliefDistributionListInfo.getStatus())) {
            reliefDistributionList = new ArrayList<>();
            reliefDistributionList = reliefDistributionListInfo.getDistributed_list();
            if (reliefDistributionList != null && reliefDistributionList.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                linearLayoutNoContent.setVisibility(View.GONE);
                if (adapter == null) {
                    adapter = new ReliefDistributionListAdapter(activity, this, reliefDistributionList);
                    recyclerView.setAdapter(adapter);
                } else {
                    adapter.updateData(reliefDistributionList);
                    adapter.notifyDataSetChanged();
                }
            } else {
                recyclerView.setVisibility(View.GONE);
                linearLayoutNoContent.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void distributionInfoFailed(String message) {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        linearLayoutNoContent.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.floatingActionButton)
    void OpenNewDistributionPage() {
        startActivity(new Intent(activity, ReliefDistributionAddActivity.class).putExtra(TAG_WHICH_OPTION_SELECTED, RELIEF_DISTRIBUTION_NEW));
    }

    @Override
    public void onItemClick(View View, int position, ReliefDistribution reliefDistribution) {
        if (reliefDistribution != null) {
            if (reliefDistribution.getStatus() == null || reliefDistribution.getStatus().isEmpty()) {
                new MyCustomToast(CFNApplication.getAppContext()).showToast("Error occurred!");
                return;
            }

            switch (reliefDistribution.getStatus()) {
                case RELIEF_DISTRIBUTION_PENDING:
                    Intent i = new Intent(this, ReliefDistributionAddActivity.class);
                    i.putExtra(TAG_SELECTED_RELIEF_DISTRIBUTION, reliefDistribution);
                    i.putExtra(TAG_WHICH_OPTION_SELECTED, reliefDistribution.getStatus());
                    startActivity(i);
                    break;
                case RELIEF_DISTRIBUTION_DONE:
                    showDistributionDetailsPopup(reliefDistribution);
                    break;
                case RELIEF_DISTRIBUTION_CANCEL:
                    new MyCustomToast(CFNApplication.getAppContext()).showToast("Already Cancelled!");
                    break;
            }
        }

    }

    private void showDistributionDetailsPopup(ReliefDistribution reliefDistribution) {

        Dialog distributionDetailsDialog = new Dialog(this.activity);
        distributionDetailsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        distributionDetailsDialog.setContentView(R.layout.custom_distribution_details_popup);
        distributionDetailsDialog.setCanceledOnTouchOutside(true);
        distributionDetailsDialog.setCancelable(true);
        distributionDetailsDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        TextView txtViewArea = distributionDetailsDialog.findViewById(R.id.txt_view_area);
        TextView txtViewAddress = distributionDetailsDialog.findViewById(R.id.txt_view_address_data);
        TextView txtViewReliefDetails = distributionDetailsDialog.findViewById(R.id.txt_view_relief_details_data);
        TextView txtViewDistributedFamily = distributionDetailsDialog.findViewById(R.id.txt_view_dis_fam_data);
        TextView txtViewFamilySurviveDay = distributionDetailsDialog.findViewById(R.id.txt_view_no_of_days_fam_sur_data);
        TextView txtViewFamilyNeedsToCover = distributionDetailsDialog.findViewById(R.id.txt_view_fam_needs_to_cover_data);


        txtViewArea.setText("Area : " + reliefDistribution.getUpazila_name());
        txtViewAddress.setText(reliefDistribution.getAddress());
        txtViewReliefDetails.setText(reliefDistribution.getReleife_items());
        txtViewDistributedFamily.setText(reliefDistribution.getNo_of_family());
        txtViewFamilySurviveDay.setText(reliefDistribution.getSurvival_day());
        txtViewFamilyNeedsToCover.setText(reliefDistribution.getIs_needed_detials());

        TextView txtViewOk = distributionDetailsDialog.findViewById(R.id.txt_view_ok);

        txtViewOk.setOnClickListener(view -> {

            if (distributionDetailsDialog != null && distributionDetailsDialog.isShowing()) {
                distributionDetailsDialog.dismiss();
            }
        });

        distributionDetailsDialog.show();

    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (controller != null)
            controller.removeListener();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

}