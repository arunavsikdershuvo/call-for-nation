package com.mobioapp.callfornation

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.setPadding


class ProgressDialog(context: Context) : Dialog(context) {

    val message: TextView = TextView(context)
    val progressBar = ProgressBar(context)
    private var container = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER
    }

    init {

        message.text = context.getString(R.string.loading)
        message.layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            this.marginStart = message.getDp(8)
        }

        container.addView(progressBar)
        container.addView(message)
        container.setPadding(container.getDp(16))


        setContentView(container)
        this.setOnDismissListener { }
        this.setCanceledOnTouchOutside(false)
    }
}