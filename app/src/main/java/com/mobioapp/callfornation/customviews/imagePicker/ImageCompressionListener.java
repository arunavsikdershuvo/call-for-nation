package com.mobioapp.callfornation.customviews.imagePicker;

public interface ImageCompressionListener {

    void onStart();

    void onCompressed(String filePath);
}
