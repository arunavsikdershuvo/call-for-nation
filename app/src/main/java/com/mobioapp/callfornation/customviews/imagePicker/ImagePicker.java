package com.mobioapp.callfornation.customviews.imagePicker;

import android.app.Activity;
import android.net.Uri;

public class ImagePicker {
    private Activity activity;
    private boolean isCompress = true;
    private ImageCompressionListener imageCompressionListener;

    public ImagePicker withActivity(Activity activity) {
        this.activity = activity;
        return this;
    }

    public ImagePicker withCompression(boolean isCompress) {
        this.isCompress = isCompress;
        return this;
    }

    public String getCompressedImagePath(Uri contentUri) {
        if (!isCompress)
            return contentUri.getPath();
        else {
            new ImageCompression(activity,
                    contentUri.getPath(), imageCompressionListener).execute();
            return null;
        }
    }

    public void addOnCompressListener(ImageCompressionListener imageCompressionListener) {
        this.imageCompressionListener = imageCompressionListener;
    }
}
