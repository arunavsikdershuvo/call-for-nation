package com.mobioapp.callfornation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mobioapp.callfornation.map.view.MapsFragment;

public class BottomNavigationFragment extends Fragment {

    private BottomNavigationView bottomNavigationView;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bottom_navigation, container, false);

        initViews(view);
        setListeners();
        switchToDashboardFragment();

        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void setListeners() {

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    if (bottomNavigationView.getSelectedItemId() != R.id.navigation_dashboard)
                        switchToDashboardFragment();
                    break;
                case R.id.navigation_map_view:
                    switchToMapViewFragment();
                    break;
            }
            return true;
        });

    }

    private void initViews(View view) {

        bottomNavigationView = view.findViewById(R.id.bottom_navigation);

    }

    private void switchToDashboardFragment() {
        if (getActivity() != null) {
            FragmentManager manager = getActivity().getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.frame_container, new DashboardFragment()).commit();
        }
    }

    private void switchToMapViewFragment() {
//        new MyCustomToast(CFNApplication.getAppContext()).showToast("Under Development");
        FragmentManager manager = getActivity().getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.frame_container, new MapsFragment()).commit();
    }

}
