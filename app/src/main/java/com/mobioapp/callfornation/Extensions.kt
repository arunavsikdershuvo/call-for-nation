package com.mobioapp.callfornation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.DisplayMetrics
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


fun AppCompatActivity.showShortToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun AppCompatActivity.showLongToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Float.formatTwoDigitLimit() = "%.2f".format(this)

fun Context.launchUrlInWeb(url: String) {
    this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
}


fun View.disable() {
    this.alpha = .5f
    isEnabled = false
}

fun View.enable() {
    this.alpha = 1f
    isEnabled = true
}

fun View.getDp(amount: Int): Int {
    val scale = context.resources.displayMetrics.density
    return (amount * scale + 0.5f).toInt()
}


fun Activity.getScreenWidth(): Int {
    val displayMetrics = DisplayMetrics()
    this.windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun Activity.getScreenHeight(): Int {
    val displayMetrics = DisplayMetrics()
    this.windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

