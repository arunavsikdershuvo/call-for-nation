package com.mobioapp.callfornation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mobioapp.callfornation.adapter.ReliefDistributionTotalListAdapter;
import com.mobioapp.callfornation.controller.ReliefDistributionListController;
import com.mobioapp.callfornation.listener.ReliefDistributionListListener;
import com.mobioapp.callfornation.model.ReliefDistribution;
import com.mobioapp.callfornation.model.ReliefDistributionListInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;
import com.mobioapp.callfornation.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_ALL;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_DONE;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_PENDING;
import static com.mobioapp.callfornation.utils.Utils.isOnline;
import static java.lang.String.format;

public class ReliefDistributionTotalListActivity extends AppCompatActivity implements ReliefDistributionListListener, ReliefDistributionTotalListAdapter.OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.linear_no_content)
    LinearLayout linearLayoutNoContent;

    private Activity activity;

    private ReliefDistributionTotalListAdapter adapter;
    private List<ReliefDistribution> reliefDistributionList = null;
    private ReliefDistributionListController controller;

    private boolean dateSortBoolAscending = false;
    private boolean isFirstTime = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relief_distribution_total_list);
        ButterKnife.bind(this);

        setToolbar();
        initViews();
        loadData(RELIEF_DISTRIBUTION_ALL);

    }

    private void loadData(String status) {

        isFirstTime = true;
        dateSortBoolAscending = false;

        progressBar.setVisibility(View.VISIBLE);
        controller = new ReliefDistributionListController(this);
        controller.startStatus(status);

    }

    private void initViews() {

        activity = ReliefDistributionTotalListActivity.this;

        if (reliefDistributionList == null)
            reliefDistributionList = new ArrayList<>();

        adapter = new ReliefDistributionTotalListAdapter(activity, this, reliefDistributionList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void setToolbar() {

        if (toolbar != null)
            toolbar.setTitle(getResources().getString(R.string.total_dis));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onItemClick(View View, int position, ReliefDistribution reliefDistribution) {

        showDistributionDetailsPopup(reliefDistribution);

    }

    @Override
    public void distributionInfoCompleted(ReliefDistributionListInfo reliefDistributionListInfo) {

        progressBar.setVisibility(View.GONE);
        if (reliefDistributionListInfo != null) {
            reliefDistributionList = new ArrayList<>();
            reliefDistributionList = reliefDistributionListInfo.getDistribution_list();

            if (reliefDistributionList != null && reliefDistributionList.size() > 0) {
                if (recyclerView.getVisibility() == View.GONE)
                    recyclerView.setVisibility(View.VISIBLE);
                if (linearLayoutNoContent.getVisibility() == View.VISIBLE)
                    linearLayoutNoContent.setVisibility(View.GONE);
                adapter.updateData(reliefDistributionList);
                adapter.notifyDataSetChanged();
            } else {
                recyclerView.setVisibility(View.GONE);
                linearLayoutNoContent.setVisibility(View.VISIBLE);
            }

        }

    }

    @Override
    public void distributionInfoFailed(String message) {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        linearLayoutNoContent.setVisibility(View.VISIBLE);
        new MyCustomToast(CFNApplication.getAppContext()).showToast("Problem receiving request");
    }

    private void showDistributionDetailsPopup(ReliefDistribution reliefDistribution) {

        Dialog distributionDetailsDialog = new Dialog(this.activity);
        distributionDetailsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        distributionDetailsDialog.setContentView(R.layout.custom_distribution_details_popup);
        distributionDetailsDialog.setCanceledOnTouchOutside(true);
        distributionDetailsDialog.setCancelable(true);
        distributionDetailsDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        TextView txtViewArea = distributionDetailsDialog.findViewById(R.id.txt_view_area);
        TextView txtViewAddress = distributionDetailsDialog.findViewById(R.id.txt_view_address_data);
        TextView txtViewReliefDetails = distributionDetailsDialog.findViewById(R.id.txt_view_relief_details_data);
        TextView txtViewDistributedFamily = distributionDetailsDialog.findViewById(R.id.txt_view_dis_fam_data);
        TextView txtViewFamilySurviveDay = distributionDetailsDialog.findViewById(R.id.txt_view_no_of_days_fam_sur_data);
        TextView txtViewFamilyNeedsToCover = distributionDetailsDialog.findViewById(R.id.txt_view_fam_needs_to_cover_data);


        txtViewArea.setText("Area : " + reliefDistribution.getUpazila_name());
        txtViewAddress.setText(reliefDistribution.getAddress());
        txtViewReliefDetails.setText(reliefDistribution.getReleife_items());
        txtViewDistributedFamily.setText(reliefDistribution.getNo_of_family());
        txtViewFamilySurviveDay.setText(reliefDistribution.getSurvival_day());
        txtViewFamilyNeedsToCover.setText(reliefDistribution.getIs_needed_detials());

        TextView txtViewOk = distributionDetailsDialog.findViewById(R.id.txt_view_ok);

        txtViewOk.setOnClickListener(view -> {

            if (distributionDetailsDialog != null && distributionDetailsDialog.isShowing()) {
                distributionDetailsDialog.dismiss();
            }
        });

        distributionDetailsDialog.show();

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (controller != null)
            controller.removeListener();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.clear();
        getMenuInflater().inflate(R.menu.menu_request_distribution, menu);

        if (menu instanceof MenuBuilder) {
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }

        ImageButton itemFilter = menu.findItem(R.id.action_settings).getActionView().findViewById(R.id.btn_menu);
        itemFilter.setOnClickListener(v -> {
            LayoutInflater inflater1 = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            final View customView = inflater1.inflate(R.layout.popup_relief_distribution_filter_menu, null);


            ImageView imageViewDownArrow = customView.findViewById(R.id.imageViewDownArrow);
            if (!isFirstTime && !dateSortBoolAscending) imageViewDownArrow.setRotation(imageViewDownArrow.getRotation() + 180);


            final PopupWindow mPopupWindow = new PopupWindow(activity);
            mPopupWindow.setContentView(customView);
            mPopupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            Display display = getWindowManager().getDefaultDisplay();
            int width = display.getWidth();
            mPopupWindow.setWidth((width*3)/4);
            mPopupWindow.setFocusable(true);
            mPopupWindow.setElevation(5.0f);
            mPopupWindow.setBackgroundDrawable(null);

            mPopupWindow.showAsDropDown(v, 0, 0);

            TextView textViewAll = customView.findViewById(R.id.textViewAll);
            TextView textViewDistributed = customView.findViewById(R.id.textViewDistributed);
            TextView textViewPending = customView.findViewById(R.id.textViewPending);
            LinearLayout linearLayoutDateSort = customView.findViewById(R.id.linear_layout_date_sort);

            textViewAll.setOnClickListener(v1 -> {

                if (!isOnline(activity)) {
                    new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
                    return;
                }

                loadData(RELIEF_DISTRIBUTION_ALL);
                mPopupWindow.dismiss();

            });

            textViewDistributed.setOnClickListener(v2 -> {
                if (!isOnline(activity)) {
                    new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
                    return;
                }

                loadData(RELIEF_DISTRIBUTION_DONE);
                mPopupWindow.dismiss();
            });

            textViewPending.setOnClickListener(v12 -> {
                if (!isOnline(activity)) {
                    new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
                    return;
                }

                loadData(RELIEF_DISTRIBUTION_PENDING);
                mPopupWindow.dismiss();
            });

            linearLayoutDateSort.setOnClickListener(v13 -> {

                if (reliefDistributionList == null || reliefDistributionList.size() == 0) {
                    mPopupWindow.dismiss();
                    return;
                }

                if (dateSortBoolAscending) {
                    Collections.sort(reliefDistributionList, (o1, o2) -> {
                        if (o1.getDate_of_distribution() == null || o2.getDate_of_distribution() == null)
                            return 0;
                        return o2.getDate_of_distribution().compareTo(o1.getDate_of_distribution());
                    });
                    dateSortBoolAscending = false;
                } else {
                    Collections.sort(reliefDistributionList, (o1, o2) -> {
                        if (o1.getDate_of_distribution() == null || o2.getDate_of_distribution() == null)
                            return 0;
                        return o1.getDate_of_distribution().compareTo(o2.getDate_of_distribution());
                    });
                    dateSortBoolAscending = true;
                }

                if (adapter != null) {
                    adapter.updateData(reliefDistributionList);
                    adapter.notifyDataSetChanged();
                }

                isFirstTime = false;
                mPopupWindow.dismiss();
            });

        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

}