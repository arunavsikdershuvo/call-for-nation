package com.mobioapp.callfornation.configuration;

public class AppConfig {

    public static final String BASE_SERVER_URL = getBaseURL();

    public static final boolean IS_DEBUGGABLE = true;

    public static String getBaseURL() {
        return "http://206.189.197.151/cfn/";
    }

}
