package com.mobioapp.callfornation;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;

import com.mobioapp.callfornation.controller.DashboardScoreboardController;
import com.mobioapp.callfornation.listener.DashboardScoreboardListener;
import com.mobioapp.callfornation.model.DashboardScoreboardInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import net.futuredrama.jomaceld.circularpblib.CircularProgressBarView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.lang.String.format;

public class DashboardFragment extends Fragment implements DashboardScoreboardListener {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.pbar)
    CircularProgressBarView circularProgressBarView;

    @BindView(R.id.card_view_distribute_relief)
    CardView cardViewDistRel;

    @BindView(R.id.txt_view_total_donors_count)
    TextView textViewTotalDonorsCount;

    @BindView(R.id.txt_view_total_relief_req_count)
    TextView textViewTotalReliefReqCount;

    @BindView(R.id.txt_view_total_feedback_count)
    TextView textViewTotalFeedbackCount;

    @BindView(R.id.txt_view_total_distribution_count)
    TextView textViewTotalDistributionCount;

    @BindView(R.id.textViewCountProgress)
    TextView textViewCountProgress;

    @BindView(R.id.textViewSubtitle)
    TextView textViewSubtitle;

    @BindView(R.id.card_view_feedback)
    CardView cardViewFeedback;

    private MutableLiveData<DashboardScoreboardInfo> scoreboardInfoMutableLiveData = new MutableLiveData<>();
    private DashboardScoreboardController controller;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        loadDashboardData();

//        loadProgressBar();
    }

    @Override
    public void onResume() {
        super.onResume();

        loadDashboardData();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (controller != null)
            controller.removeListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        scoreboardInfoMutableLiveData.observe(getViewLifecycleOwner(), this::onScoreCardComplete);
    }

    private void loadProgressBar(Float progress) {

        circularProgressBarView.setNumberOfBars(1);
        circularProgressBarView.setBarsColors(new int[]{getResources().getColor(R.color.bar_color)});
        circularProgressBarView.setProgressWithAnimation(progress);
    }

    private void loadDashboardData() {

        progressBar.setVisibility(View.VISIBLE);
        controller = new DashboardScoreboardController(this);
        controller.start();

    }

    @OnClick(R.id.card_view_feedback)
    void openFeedback() {
        AlertDialog feedbackDialog = new AlertDialog.Builder(getContext())
                .setView(R.layout.dialog_feedback_layout)
                .setPositiveButton(R.string.submit_feedback, (dialog, which) -> {
                    startActivity(new Intent(getContext(), FeedbackActivity.class));
//                    EditText feedbackET = ((AlertDialog) dialog).findViewById(R.id.et_feedback);
//                    String contents = feedbackET.getText().toString();
//                    if (contents.isEmpty())
//                        Toast.makeText(getContext(), R.string.no_empty_feedback, Toast.LENGTH_SHORT).show();
//                    else
//                        dashboardViewModel.submitFeedback(contents);
                })
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {

                })
                .create();
        feedbackDialog.show();

    }

    @OnClick(R.id.card_view_donors)
    void gotToTotalOrganisationPage() {
        startActivity(new Intent(getActivity(), TotalOrganisationActivity.class));
    }

    @OnClick(R.id.card_view_relief_request)
    void gotToAlreadyDistributionPage() {
        startActivity(new Intent(getActivity(), ReliefRequestListActivity.class));
    }

    @OnClick(R.id.card_view_distribute_relief)
    void goToReliefDistributionListActivity() {
        startActivity(new Intent(getActivity(), ReliefDistributionListActivity.class));
    }

    @OnClick(R.id.card_total_distribution)
    void goToReliefDistributionTotalListActivity() {
        startActivity(new Intent(getActivity(), ReliefDistributionTotalListActivity.class));
    }

    @Override
    public void loadScoreboardCompleted(DashboardScoreboardInfo dashboardScoreboardInfo) {
        scoreboardInfoMutableLiveData.postValue(dashboardScoreboardInfo);
    }

    public void onScoreCardComplete(DashboardScoreboardInfo dashboardScoreboardInfo) {
        progressBar.setVisibility(View.GONE);
        if (dashboardScoreboardInfo != null) {
            if (dashboardScoreboardInfo.getDashboard() != null && dashboardScoreboardInfo.getDashboard().size() > 0) {
//                int totalFamilyFoodHave = dashboardScoreboardInfo.getDashboard().get(0).getTotal_family_food_have();
                int totalFamilyFoodDontHave = dashboardScoreboardInfo.getDashboard().get(0).getTotal_family_food_needed() - dashboardScoreboardInfo.getDashboard().get(0).getTotal_family_food_have();
                int totalFamilyFoodNeeded = dashboardScoreboardInfo.getDashboard().get(0).getTotal_family_food_needed();
                loadProgressBar((float) totalFamilyFoodDontHave / totalFamilyFoodNeeded);
                textViewCountProgress.setText(totalFamilyFoodDontHave + "/" + totalFamilyFoodNeeded);
                textViewSubtitle.setText(Html.fromHtml("<b>" + totalFamilyFoodDontHave
                        + "</b> out of <b>" + totalFamilyFoodNeeded + "</b> families <br>don't have today's food"));
                textViewTotalDonorsCount.setText(format("%02d", Integer.parseInt(dashboardScoreboardInfo.getDashboard().get(0).getTotal_user())));
                textViewTotalReliefReqCount.setText(format("%02d", Integer.parseInt(dashboardScoreboardInfo.getDashboard().get(0).getTotal_relief_request())));
                textViewTotalFeedbackCount.setText(format("%02d", Integer.parseInt(dashboardScoreboardInfo.getDashboard().get(0).getTotal_feedback())));
                textViewTotalDistributionCount.setText(format("%02d", Integer.parseInt(dashboardScoreboardInfo.getDashboard().get(0).getTotal_distributed_done())));
            }
        } else progressBar.setVisibility(View.GONE);
    }

    @Override
    public void loadScoreboardFailed(String message) {
        scoreboardInfoMutableLiveData.postValue(null);
        new MyCustomToast(CFNApplication.getAppContext()).showToast("Scorecard sync failed!");
    }
}
