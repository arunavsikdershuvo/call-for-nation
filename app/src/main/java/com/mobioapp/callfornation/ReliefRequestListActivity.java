package com.mobioapp.callfornation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mobioapp.callfornation.adapter.ReliefRequestAdapter;
import com.mobioapp.callfornation.controller.ReliefRequestController;
import com.mobioapp.callfornation.listener.ReliefRequestListListener;
import com.mobioapp.callfornation.model.ReliefRequest;
import com.mobioapp.callfornation.model.ReliefRequestListInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_REQUEST_FOR_ALL;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_REQUEST_FOR_INDIVIDUAL;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_REQUEST_FOR_OTHERS;
import static com.mobioapp.callfornation.utils.Utils.isOnline;
import static java.lang.String.format;

public class ReliefRequestListActivity extends AppCompatActivity implements ReliefRequestListListener, ReliefRequestAdapter.OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.linear_no_content)
    LinearLayout linearLayoutNoContent;

    @BindView(R.id.textViewTotalCount)
    TextView textViewTotalCount;

    private Activity activity;
    private List<ReliefRequest> reliefRequestList = null;
    private ReliefRequestAdapter adapter;

    private MenuItem menuItem;

    private ReliefRequestController controller;

    private boolean dateSortBoolAscending = false;
    private boolean isFirstTime = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relief_request);
        ButterKnife.bind(this);

        setToolbar();
        initViews();

    }

    private void setToolbar() {

        if (toolbar != null)
            toolbar.setTitle(getResources().getString(R.string.relief_request));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    private void initViews() {

        activity = ReliefRequestListActivity.this;

        if (reliefRequestList == null)
            reliefRequestList = new ArrayList<>();

        adapter = new ReliefRequestAdapter(activity, this, reliefRequestList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void loadData(String type) {

        isFirstTime = true;
        dateSortBoolAscending = false;

        progressBar.setVisibility(View.VISIBLE);

        controller = new ReliefRequestController(this);
        controller.start(type);

    }

    @Override
    public void reliefRequestFetchCompleted(ReliefRequestListInfo reliefRequestListInfo) {

        progressBar.setVisibility(View.GONE);
        if (reliefRequestListInfo != null) {
            reliefRequestList = new ArrayList<>();
            this.reliefRequestList = reliefRequestListInfo.getDetials();
        }

        if (reliefRequestList != null && reliefRequestList.size() > 0) {
            if (recyclerView.getVisibility() == View.GONE)
                recyclerView.setVisibility(View.VISIBLE);
            if (linearLayoutNoContent.getVisibility() == View.VISIBLE)
                linearLayoutNoContent.setVisibility(View.GONE);
            if (textViewTotalCount.getVisibility() == View.GONE)
                textViewTotalCount.setVisibility(View.VISIBLE);
            textViewTotalCount.setText("Total Request: " + format("%02d", reliefRequestList.size()));
            adapter.updateData(reliefRequestList);
            adapter.notifyDataSetChanged();
        } else {
            recyclerView.setVisibility(View.GONE);
            linearLayoutNoContent.setVisibility(View.VISIBLE);
            textViewTotalCount.setVisibility(View.GONE);
        }

    }

    @Override
    public void reliefRequestFetchFailed(String message) {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        textViewTotalCount.setVisibility(View.GONE);
        linearLayoutNoContent.setVisibility(View.VISIBLE);
        new MyCustomToast(CFNApplication.getAppContext()).showToast("Problem receiving request");
    }

    @Override
    public void onItemClick(View View, int position, ReliefRequest reliefRequest) {

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (controller != null)
            controller.removeListener();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.clear();
        getMenuInflater().inflate(R.menu.menu_request_relief, menu);

        if (menu instanceof MenuBuilder) {
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }

        menuItem = menu.getItem(0);

        ImageButton itemNew = menu.findItem(R.id.action_new).getActionView().findViewById(R.id.btn_menu_new);
        itemNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, ReliefRequestAddActivity.class));
            }
        });
        ImageButton itemFilter = menu.findItem(R.id.action_settings).getActionView().findViewById(R.id.btn_menu);
        itemFilter.setOnClickListener(v -> {
            LayoutInflater inflater1 = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            final View customView = inflater1.inflate(R.layout.popup_relief_request_filter_menu, null);


            ImageView imageViewDownArrow = customView.findViewById(R.id.imageViewDownArrow);
            if (!isFirstTime && !dateSortBoolAscending) imageViewDownArrow.setRotation(imageViewDownArrow.getRotation() + 180);


            final PopupWindow mPopupWindow = new PopupWindow(activity);
            mPopupWindow.setContentView(customView);
            mPopupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            Display display = getWindowManager().getDefaultDisplay();
            int width = display.getWidth();
            mPopupWindow.setWidth((width*3)/4);
            mPopupWindow.setFocusable(true);
            mPopupWindow.setElevation(5.0f);
            mPopupWindow.setBackgroundDrawable(null);

            mPopupWindow.showAsDropDown(v, 0, 0);

            TextView textViewAll = customView.findViewById(R.id.textViewAll);
            TextView textViewIndividual = customView.findViewById(R.id.textViewIndividual);
            TextView textViewOthers = customView.findViewById(R.id.textViewOthers);
            LinearLayout linearLayoutDateSort = customView.findViewById(R.id.linear_layout_date_sort);

            textViewAll.setOnClickListener(v1 -> {

                if (!isOnline(activity)) {
                    new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
                    return;
                }

                loadData(RELIEF_REQUEST_FOR_ALL);
                mPopupWindow.dismiss();

            });

            textViewIndividual.setOnClickListener(v2 -> {
                if (!isOnline(activity)) {
                    new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
                    return;
                }

                loadData(RELIEF_REQUEST_FOR_INDIVIDUAL);
                mPopupWindow.dismiss();
            });

            textViewOthers.setOnClickListener(v12 -> {
                if (!isOnline(activity)) {
                    new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
                    return;
                }

                loadData(RELIEF_REQUEST_FOR_OTHERS);
                mPopupWindow.dismiss();
            });

            linearLayoutDateSort.setOnClickListener(v13 -> {

                if (reliefRequestList == null || reliefRequestList.size() == 0) {
                    mPopupWindow.dismiss();
                    return;
                }

                if (dateSortBoolAscending) {
                    Collections.sort(reliefRequestList, (o1, o2) -> {
                        if (o1.getCreated_at() == null || o2.getCreated_at() == null)
                            return 0;
                        return o2.getCreated_at().compareTo(o1.getCreated_at());
                    });
                    dateSortBoolAscending = false;
                } else {
                    Collections.sort(reliefRequestList, (o1, o2) -> {
                        if (o1.getCreated_at() == null || o2.getCreated_at() == null)
                            return 0;
                        return o1.getCreated_at().compareTo(o2.getCreated_at());
                    });
                    dateSortBoolAscending = true;
                }

                if (adapter != null) {
                    adapter.updateData(reliefRequestList);
                    adapter.notifyDataSetChanged();
                }

                isFirstTime = false;
                mPopupWindow.dismiss();
            });

        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData(RELIEF_REQUEST_FOR_ALL);

    }
}
