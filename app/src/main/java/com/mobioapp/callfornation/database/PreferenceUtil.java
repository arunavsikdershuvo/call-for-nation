package com.mobioapp.callfornation.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobioapp.callfornation.model.LoginInfo;
import com.mobioapp.callfornation.utils.ConstantValues;

public class PreferenceUtil {

    public SharedPreferences sharedPreferences;
    private SharedPreferences.Editor spEditor;
    private static Gson gson;

    public PreferenceUtil(Context mContext) {
        super();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        if (gson == null)
            gson = new GsonBuilder()
                    .setLenient()
                    .create();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(ConstantValues.LOGIN_TAG, false);
    }

    public void setLogin(boolean login) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(ConstantValues.LOGIN_TAG, login);
        spEditor.apply();
    }


    public String getSelectedLangShortCode() {
        return sharedPreferences.getString(ConstantValues.LANG_SHORT_CODE_TAG, "en");
    }

    public void setSelectedLangShortCode(String code) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(ConstantValues.LANG_SHORT_CODE_TAG, code);
        spEditor.apply();
    }

    public boolean getLangBool() {
        return sharedPreferences.getBoolean(ConstantValues.LANG_TAG, false);
    }

    public void setLangBool(boolean bool) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(ConstantValues.LANG_TAG, bool);
        spEditor.apply();
    }

    public boolean isFirstTimeInApp() {
        return sharedPreferences.getBoolean(ConstantValues.IS_FIRST_TIME_APP, true);
    }

    public void setFirstTimeInApp(boolean bool) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(ConstantValues.IS_FIRST_TIME_APP, bool);
        spEditor.apply();
    }

    public LoginInfo getUserDetails() {
       return retrieveLoginData();
    }

    public void clearLoginData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ConstantValues.USER_LOGIN_TAG, null);
        editor.apply();
    }

    public void updateLoginData(LoginInfo userDetails) {
        String loginJson = gson.toJson(userDetails);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ConstantValues.USER_LOGIN_TAG, loginJson);
        editor.apply();
    }

    public LoginInfo retrieveLoginData() {
        String json = sharedPreferences.getString(ConstantValues.USER_LOGIN_TAG, null);
        if (json == null) return null;
        return gson.fromJson(json, LoginInfo.class);
    }

}