package com.mobioapp.callfornation.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mobioapp.callfornation.controller.AddUpazillaListController
import com.mobioapp.callfornation.database.PreferenceUtil
import com.mobioapp.callfornation.listener.AddUpazillaListListener
import com.mobioapp.callfornation.listener.FeedbackApi
import com.mobioapp.callfornation.map.network.RetrofitClient
import com.mobioapp.callfornation.model.FeedbackDto
import com.mobioapp.callfornation.model.UpazillaInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DashboardViewModel(application: Application) : AndroidViewModel(application), AddUpazillaListListener {

    private val feedbackApi by lazy { RetrofitClient.retrofitInstance?.create(FeedbackApi::class.java) }
    private val addUpazillaListController by lazy { AddUpazillaListController(this) }
    private val feedbackSubmissionStatus = MutableLiveData<Int>()
    val upazilaList = MutableLiveData<Map<String, String>>()

    fun submitFeedback(title: String?, upozillaId: String?, feedback: String?) {
        feedbackSubmissionStatus.postValue(-1)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val feedbackDto = FeedbackDto()
                feedbackDto.userId = PreferenceUtil(getApplication()).retrieveLoginData()?.user_id
                feedbackDto.feedback = "$title\n$feedback"
                feedbackDto.upazilaId = upozillaId

                val response = feedbackApi?.submitFeedback(feedbackDto)?.execute()

                if (response?.isSuccessful == true)
                    feedbackSubmissionStatus.postValue(1)
                else feedbackSubmissionStatus.postValue(0)
            } catch (e: Exception) {
                e.printStackTrace()
                feedbackSubmissionStatus.postValue(0)
            }

        }
    }

    fun requestUpazillaList() {
        addUpazillaListController.start()
    }

    fun getFeedbackSubmissionStatus(): LiveData<Int> = feedbackSubmissionStatus

    override fun addUpazillaListFailed(message: String?) {
        upazilaList.postValue(null)
    }

    override fun addUpazillaListCompleted(upazillaInfo: UpazillaInfo?) {
        if (upazillaInfo?.upazila_list == null)
            return upazilaList.postValue(null)

        viewModelScope.launch(Dispatchers.IO) {
            val result = upazillaInfo.upazila_list.map { it.upazila_name to it.id }.toMap()
            upazilaList.postValue(result)
        }

    }
}