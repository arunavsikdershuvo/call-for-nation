package com.mobioapp.callfornation

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputLayout
import com.mobioapp.callfornation.utils.CurrentLocationFetcher
import com.mobioapp.callfornation.utils.MyCustomToast
import com.mobioapp.callfornation.viewmodel.DashboardViewModel
import kotlinx.android.synthetic.main.activity_feedback.*

class FeedbackActivity : AppCompatActivity() {

    private val viewModel by lazy { ViewModelProviders.of(this)[DashboardViewModel::class.java] }
    private val currentLocationFetcher by lazy { CurrentLocationFetcher(this) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)

        toolbar.title = "Feedback"

        setSupportActionBar(toolbar)

        toolbar.setNavigationOnClickListener { onBackPressed() }

//        currentLocationFetcher.currentAddressLiveData.observe(this, Observer { address ->
//            if (!address.isNullOrEmpty())
//                et_area.setText(address)
//        })


//        Dexter.withContext(this)
//                .withPermissions(
//                        Manifest.permission.ACCESS_FINE_LOCATION
//                ).withListener(object : MultiplePermissionsListener {
//                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
//                        if (report.areAllPermissionsGranted()) {
//                            run {
//                                val successful = currentLocationFetcher.fetchOnce()
//                                Log.d("LocationFetch", successful.toString() + "")
//                            }
//                        }
//                    }
//
//                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {}
//                }).check()


        initViewModel()
        initView()
        initListener()
    }

    lateinit var upozillaMap: Map<String, String>

    private fun initViewModel() {
        viewModel.getFeedbackSubmissionStatus().observe(this, Observer { status ->
            when (status) {
                -1 -> {
                    progress_bar2.visibility = View.VISIBLE
                    showToast(getString(R.string.submiting))
                }
                1 -> {
                    progress_bar2.visibility = View.GONE
                    showToast(getString(R.string.feedback_success))
                }
                0 -> {
                    progress_bar2.visibility = View.GONE
                    showToast(getString(R.string.feedbac_failed))
                }
            }
        })

        viewModel.upazilaList.observe(this, Observer { upazillaMap ->
            if (upazillaMap == null)
                showToast("Upozilla List failed")
            else {
                this.upozillaMap = upazillaMap;
                val sortedList = upazillaMap.keys.toList().sorted()
                val spinnerArrayAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                        sortedList)
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner_area.adapter = spinnerArrayAdapter
                spinner_area.setSelection(0)
            }
            progress_bar2.visibility = View.GONE
        })
        progress_bar2.visibility = View.VISIBLE
        viewModel.requestUpazillaList()
    }

    private fun initListener() {
        bt_feedback.setOnClickListener {
            val titleValidated = checkEditText(et_title, outlinedTitle)
            val areaValidated = spinner_area.selectedItem.run {
                this != null
            }

            if (areaValidated && ::upozillaMap.isInitialized)
                outlinedArea.isErrorEnabled = false
            else {
                outlinedArea.isErrorEnabled = true
                outlinedArea.error = "No area selected"
            }

            val descriptionValidated = checkEditText(et_description, outlinedDescription)

            if (titleValidated && areaValidated && descriptionValidated) {
                viewModel.submitFeedback(et_title.text.toString(),
                        upozillaMap.get(spinner_area.selectedItem.toString()),
                        et_description.text.toString())
            }
        }
    }

    private fun initView() {

    }

    private fun checkEditText(editText: EditText, textInputLayout: TextInputLayout): Boolean {
        return if (editText.text?.isEmpty() != false) {
            textInputLayout.isErrorEnabled = true
            textInputLayout.error = "This field can't be empty"
            false
        } else {
            textInputLayout.isErrorEnabled = false
            true
        }

    }
}

public fun AppCompatActivity.showToast(string: String) {
    MyCustomToast(this.applicationContext).showToast(string)
}