package com.mobioapp.callfornation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.model.ReliefDistribution;
import com.mobioapp.callfornation.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReliefDistributionTotalListAdapter extends RecyclerView.Adapter<ReliefDistributionTotalListAdapter.DistributionInformationHolder> {

    private Context context;
    private List<ReliefDistribution> reliefDistributionList;
    private OnItemClickListener listener;

    public ReliefDistributionTotalListAdapter(Context context, OnItemClickListener listener, List<ReliefDistribution> reliefDistributionList) {
        this.context = context;
        this.listener = listener;
        this.reliefDistributionList = reliefDistributionList;
    }

    public void updateData(List<ReliefDistribution> reliefDistributionList) {
        this.reliefDistributionList = reliefDistributionList;
    }

    @Override
    public DistributionInformationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.relief_distribution_total_list_row, parent, false);
        return new ReliefDistributionTotalListAdapter.DistributionInformationHolder(view);
    }

    @Override
    public void onBindViewHolder(ReliefDistributionTotalListAdapter.DistributionInformationHolder holder, int position) {
        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();

        ReliefDistribution reliefDistribution = reliefDistributionList.get(position);

        holder.textViewOrgName.setText(("").equals(reliefDistribution.getUser_name()) ? "Not found" : reliefDistribution.getUser_name());
        holder.textViewDate.setText(Utils.convertDateToHumanReadableFromServer(reliefDistribution.getDate_of_distribution()));
        holder.textViewStatus.setText(Utils.getStatusNameForDistribution(reliefDistribution.getStatus()));
        holder.textViewStatus.setTextColor(Utils.getTextColorForDistributionStatus(context, reliefDistribution.getStatus()));
        holder.textViewUpazillaName.setText(("").equals(reliefDistribution.getUpazila_name()) ? "Not Found" : reliefDistribution.getUpazila_name());
        holder.textViewFamCovered.setText(reliefDistribution.getNo_of_family());

    }

    @Override
    public int getItemCount() {
        return reliefDistributionList != null && reliefDistributionList.size() > 0 ? reliefDistributionList.size() : 0;
    }

    public interface OnItemClickListener {
        void onItemClick(View View, int position, ReliefDistribution reliefDistribution);
    }

    public class DistributionInformationHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_org_name)
        TextView textViewOrgName;

        @BindView(R.id.text_view_dist_date)
        TextView textViewDate;

        @BindView(R.id.text_view_status)
        TextView textViewStatus;

        @BindView(R.id.text_view_upazilla_name)
        TextView textViewUpazillaName;

        @BindView(R.id.text_view_no_of_fam_count)
        TextView textViewFamCovered;

        public DistributionInformationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(v, DistributionInformationHolder.this.getAdapterPosition(), reliefDistributionList.get(DistributionInformationHolder.this.getAdapterPosition()));
                }
            });

        }
    }
}
