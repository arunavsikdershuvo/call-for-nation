package com.mobioapp.callfornation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.model.Organisation;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TotalOrganisationAdapter extends RecyclerView.Adapter<TotalOrganisationAdapter.TotalOrganisationListHolder> {

    private Context context;
    private List<Organisation> organisationList;
    private OnItemClickListener listener;

    public TotalOrganisationAdapter(Context context, OnItemClickListener listener, List<Organisation> organisationList) {
        this.context = context;
        this.listener = listener;
        this.organisationList = organisationList;
    }

    public void updateData(List<Organisation> organisationList) {
        this.organisationList = organisationList;
    }

    @Override
    public TotalOrganisationListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.organisation_list_row, parent, false);
        return new TotalOrganisationAdapter.TotalOrganisationListHolder(view);
    }

    @Override
    public void onBindViewHolder(TotalOrganisationListHolder holder, int position) {
        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();

        holder.textViewOrgName.setText(organisationList.get(position).user_name);
        holder.textViewOrgAddress.setText(organisationList.get(position).user_email);
        holder.textViewOrgMobile.setText(organisationList.get(position).ueser_phone);

    }

    @Override
    public int getItemCount() {
        return organisationList != null && organisationList.size() > 0 ? organisationList.size() : 0;
    }

    public interface OnItemClickListener {
        void onItemClick(View View, int position, Organisation organisation);
    }

    public class TotalOrganisationListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_org_name)
        TextView textViewOrgName;

        @BindView(R.id.text_view_org_address)
        TextView textViewOrgAddress;

        @BindView(R.id.text_view_org_mobile)
        TextView textViewOrgMobile;

        public TotalOrganisationListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(v, TotalOrganisationListHolder.this.getAdapterPosition(), organisationList.get(TotalOrganisationListHolder.this.getAdapterPosition()));
                }
            });

        }
    }
}
