package com.mobioapp.callfornation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.model.ReliefRequest;
import com.mobioapp.callfornation.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReliefRequestAdapter extends RecyclerView.Adapter<ReliefRequestAdapter.ReliefRequestHolder> {

    private Context context;
    private List<ReliefRequest> reliefRequestList;
    private OnItemClickListener listener;

    public ReliefRequestAdapter(Context context, OnItemClickListener listener, List<ReliefRequest> reliefRequestList) {
        this.context = context;
        this.listener = listener;
        this.reliefRequestList = reliefRequestList;
    }

    public void updateData(List<ReliefRequest> reliefRequestList) {
        this.reliefRequestList = reliefRequestList;
    }

    @Override
    public ReliefRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.relief_request_list_row, parent, false);
        return new ReliefRequestAdapter.ReliefRequestHolder(view);
    }

    @Override
    public void onBindViewHolder(ReliefRequestAdapter.ReliefRequestHolder holder, int position) {
        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();

        holder.textViewUserFullName.setText(reliefRequestList.get(position).getUser_full_name().equals("") ? "Name Not Found" : reliefRequestList.get(position).getUser_full_name());
        if (reliefRequestList.get(position).getType().equals("1")) {
            holder.linearLayoutFamily.setVisibility(View.VISIBLE);
            holder.linearLayoutArea.setVisibility(View.GONE);
            holder.textViewFamilyMemberNumber.setText(reliefRequestList.get(position).getNo_of_family());
        } else if (reliefRequestList.get(position).getType().equals("2")) {
            holder.linearLayoutFamily.setVisibility(View.GONE);
            holder.linearLayoutArea.setVisibility(View.VISIBLE);
            holder.textViewUAreaPopulationNumber.setText(reliefRequestList.get(position).getNo_of_family());
        }else if (reliefRequestList.get(position).getType().equals("0")) {
            holder.linearLayoutFamily.setVisibility(View.GONE);
            holder.linearLayoutArea.setVisibility(View.GONE);
        }
        holder.textViewRequestDate.setText(Utils.convertDateToHumanReadableFromServer(reliefRequestList.get(position).getCreated_at()));
        holder.textViewNIDNumber.setText(reliefRequestList.get(position).getNid().equals("") || reliefRequestList.get(position).getNid().contains("http") ? "NID Not Found" : reliefRequestList.get(position).getNid());
        holder.textViewAreaName.setText(reliefRequestList.get(position).getUpazila_name());

    }

    @Override
    public int getItemCount() {
        return reliefRequestList != null && reliefRequestList.size() > 0 ? reliefRequestList.size() : 0;
    }

    public interface OnItemClickListener {
        void onItemClick(View View, int position, ReliefRequest reliefRequest);
    }

    public class ReliefRequestHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_full_name)
        TextView textViewUserFullName;

        @BindView(R.id.linearLayoutArea)
        LinearLayout linearLayoutArea;

        @BindView(R.id.text_view_area_pop_number)
        TextView textViewUAreaPopulationNumber;

        @BindView(R.id.linearLayoutFamily)
        LinearLayout linearLayoutFamily;

        @BindView(R.id.text_view_family_member_number)
        TextView textViewFamilyMemberNumber;

        @BindView(R.id.text_view_nid_number)
        TextView textViewNIDNumber;

        @BindView(R.id.text_view_req_date)
        TextView textViewRequestDate;

        @BindView(R.id.text_view_area_name)
        TextView textViewAreaName;


        public ReliefRequestHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(v, ReliefRequestHolder.this.getAdapterPosition(), reliefRequestList.get(ReliefRequestHolder.this.getAdapterPosition()));
                }
            });

        }
    }
}
