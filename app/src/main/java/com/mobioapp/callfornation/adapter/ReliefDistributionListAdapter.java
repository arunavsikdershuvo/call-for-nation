package com.mobioapp.callfornation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.model.ReliefDistribution;
import com.mobioapp.callfornation.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReliefDistributionListAdapter extends RecyclerView.Adapter<ReliefDistributionListAdapter.DistributionInformationHolder> {

    private Context context;
    private List<ReliefDistribution> reliefDistributionList;
    private OnItemClickListener listener;

    public ReliefDistributionListAdapter(Context context, OnItemClickListener listener, List<ReliefDistribution> reliefDistributionList) {
        this.context = context;
        this.listener = listener;
        this.reliefDistributionList = reliefDistributionList;
    }

    public void updateData(List<ReliefDistribution> reliefDistributionList) {
        this.reliefDistributionList = reliefDistributionList;
    }

    @Override
    public DistributionInformationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.distribution_list_row, parent, false);
        return new ReliefDistributionListAdapter.DistributionInformationHolder(view);
    }

    @Override
    public void onBindViewHolder(ReliefDistributionListAdapter.DistributionInformationHolder holder, int position) {
        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();

        holder.getTextViewUpazillaName.setText(reliefDistributionList.get(position).getUpazila_name());
        holder.textViewReqDate.setText(Utils.convertDateToHumanReadableFromServer(reliefDistributionList.get(position).getDate_of_distribution()));
        holder.textViewStatus.setText(Utils.getStatusNameForDistribution(reliefDistributionList.get(position).getStatus()));
        holder.textViewStatus.setTextColor(Utils.getTextColorForDistributionStatus(context, reliefDistributionList.get(position).getStatus()));

    }

    @Override
    public int getItemCount() {
        return reliefDistributionList != null && reliefDistributionList.size() > 0 ? reliefDistributionList.size() : 0;
    }

    public interface OnItemClickListener {
        void onItemClick(View View, int position, ReliefDistribution reliefDistribution);
    }

    public class DistributionInformationHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_upazilla_name)
        TextView getTextViewUpazillaName;

        @BindView(R.id.text_view_req_date)
        TextView textViewReqDate;

        @BindView(R.id.text_view_status)
        TextView textViewStatus;

        public DistributionInformationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(v, DistributionInformationHolder.this.getAdapterPosition(), reliefDistributionList.get(DistributionInformationHolder.this.getAdapterPosition()));
                }
            });

        }
    }
}
