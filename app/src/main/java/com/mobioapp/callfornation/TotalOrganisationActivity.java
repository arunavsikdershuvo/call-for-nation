package com.mobioapp.callfornation;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mobioapp.callfornation.database.PreferenceUtil;
import com.mobioapp.callfornation.utils.ConstantValues;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mobioapp.callfornation.utils.ConstantValues.TAB_TITLE_TOTAL_GOVT_ORG;
import static com.mobioapp.callfornation.utils.ConstantValues.TAB_TITLE_TOTAL_INDIVIDUAL;
import static com.mobioapp.callfornation.utils.ConstantValues.TAB_TITLE_TOTAL_PRIVATE_ORG;

public class TotalOrganisationActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public static TabLayout tabs;

    @BindView(R.id.viewpagerMain)
    public ViewPager viewPager;

    private Activity activity;

    private PreferenceUtil preferenceUtil;

    public static final Integer[] TABS_TITLE = {
            TAB_TITLE_TOTAL_GOVT_ORG,
            TAB_TITLE_TOTAL_PRIVATE_ORG,
            TAB_TITLE_TOTAL_INDIVIDUAL
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_organisation);
        ButterKnife.bind(this);

        setToolbar();
        initViews();
        loadTab();
    }

    private void loadTab() {
        viewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        tabs.setupWithViewPager(viewPager);

    }

    private void setToolbar() {

        if (toolbar != null)
            toolbar.setTitle(getResources().getString(R.string.total_org));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {

        activity = TotalOrganisationActivity.this;
        preferenceUtil = new PreferenceUtil(activity);
        tabs = findViewById(R.id.tabs);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return ConstantValues.getString(activity, TABS_TITLE[position]);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            int title = TABS_TITLE[position];
            switch (title) {
                default:
                case TAB_TITLE_TOTAL_GOVT_ORG:
                    fragment = TotalOrganisationFragment.newInstance("3");
                    break;
                case TAB_TITLE_TOTAL_PRIVATE_ORG:
                    fragment = TotalOrganisationFragment.newInstance("2");
                    break;
                    case TAB_TITLE_TOTAL_INDIVIDUAL:
                    fragment = TotalOrganisationFragment.newInstance("1");
                    break;
            }

            return fragment;

        }

        @Override
        public int getCount() {
            return TABS_TITLE.length;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
