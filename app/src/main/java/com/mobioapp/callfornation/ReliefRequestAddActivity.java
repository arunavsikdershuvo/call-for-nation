package com.mobioapp.callfornation;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.mobioapp.callfornation.controller.AddReliefRequestController;
import com.mobioapp.callfornation.controller.AddUpazillaListController;
import com.mobioapp.callfornation.database.PreferenceUtil;
import com.mobioapp.callfornation.listener.AddReliefRequestListener;
import com.mobioapp.callfornation.listener.AddUpazillaListListener;
import com.mobioapp.callfornation.model.AddReliefRequestInfo;
import com.mobioapp.callfornation.model.Upazilla;
import com.mobioapp.callfornation.model.UpazillaInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mobioapp.callfornation.utils.Utils.isOnline;

public class ReliefRequestAddActivity extends AppCompatActivity implements AddReliefRequestListener, AddUpazillaListListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.spinner_area)
    Spinner spinnerUpazilla;

    @BindView(R.id.spinner_request_for)
    Spinner spinnerRequestFor;

    @BindView(R.id.etName)
    EditText editTextName;

    @BindView(R.id.etNID)
    EditText editTextNID;

    @BindView(R.id.etAddress)
    EditText editTextAddress;

    @BindView(R.id.etFamMemOrPop)
    EditText editTextFamMemOrPop;

    private Activity activity;

    private PreferenceUtil preferenceUtil;

    private AddReliefRequestController controller;
    private AddUpazillaListController controllerUpazilla;

    private List<Upazilla> upazillaList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_relief_request);
        ButterKnife.bind(this);

        setToolbar();
        initViews();

    }

    private void setToolbar() {

        if (toolbar != null)
            toolbar.setTitle(getResources().getString(R.string.new_request));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    private void initViews() {

        activity = ReliefRequestAddActivity.this;
        preferenceUtil = new PreferenceUtil(activity);

        loadSpinnerData();
        getUpazillaList();
    }

    private void getUpazillaList() {
        progressBar.setVisibility(View.VISIBLE);
        controllerUpazilla = new AddUpazillaListController(this);
        controllerUpazilla.start();

    }

    private void loadSpinnerData() {

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        getResources().getStringArray(R.array.add_relief_request_type));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerRequestFor.setAdapter(spinnerArrayAdapter);

    }

    @OnClick(R.id.btnSubmit)
    void submitReliefRequest() {

        if (!isOnline(activity)) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
            return;
        }

        if (editTextName.getText().toString().trim().length() <= 0 || editTextAddress.getText().toString().trim().length() <= 0 ||
                editTextNID.getText().toString().trim().length() <= 0) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast("Please provide all information.");
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        controller = new AddReliefRequestController(this);
        controller.start(preferenceUtil.retrieveLoginData().getUser_id(),
                getRespectiveUpazillaIdFromName(spinnerUpazilla.getSelectedItem().toString().trim()),
                (spinnerRequestFor.getSelectedItemPosition() + 1) + "",
                editTextName.getText().toString().trim(),
                editTextAddress.getText().toString().trim(),
                editTextFamMemOrPop.getText().toString().trim(),
                "", "", "", editTextNID.getText().toString().trim());
    }

    @Override
    public void addReliefRequestCompleted(AddReliefRequestInfo addReliefRequestInfo) {
        progressBar.setVisibility(View.GONE);
        if (addReliefRequestInfo != null) {
            if (addReliefRequestInfo.getStatus().equals("201")) {
                new MyCustomToast(CFNApplication.getAppContext()).showToast("Relief Request Added!");
                finish();
            } else {
                new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.there_is_a_problem));
            }
        } else {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.there_is_a_problem));
        }
    }

    @Override
    public void addReliefRequestFailed(String message) {
        progressBar.setVisibility(View.GONE);
        new MyCustomToast(CFNApplication.getAppContext()).showToast("Problem with server.");
    }

    @Override
    public void addUpazillaListCompleted(UpazillaInfo upazillaInfo) {

        progressBar.setVisibility(View.GONE);

        if (upazillaInfo != null) {
            this.upazillaList = upazillaInfo.upazila_list;

            if (upazillaList != null && upazillaList.size() > 0) {
                List<String> upazillaNameList = new ArrayList<>();

                for (int i = 0; i < upazillaList.size(); i++) {
                    upazillaNameList.add(upazillaList.get(i).upazila_name);
                }

                loadSpinnerUpazillaData(upazillaNameList);
            }
        }

    }

    @Override
    public void addUpazillaListFailed(String message) {
        progressBar.setVisibility(View.GONE);
        new MyCustomToast(CFNApplication.getAppContext()).showToast("Data syncing failed!");
    }

    private void loadSpinnerUpazillaData(List<String> upazillaNameList) {

        Collections.sort(upazillaNameList, String::compareToIgnoreCase);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        upazillaNameList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerUpazilla.setAdapter(spinnerArrayAdapter);

    }

    private String getRespectiveUpazillaIdFromName(String upazillaName) {
        for (int i = 0; i < upazillaList.size(); i++) {
            if (upazillaList.get(i).upazila_name.equalsIgnoreCase(upazillaName)) {
                return upazillaList.get(i).id;
            }
        }
        return "0";
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (controller != null)
            controller.removeListener();

        if (controllerUpazilla != null)
            controllerUpazilla.removeListener();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}