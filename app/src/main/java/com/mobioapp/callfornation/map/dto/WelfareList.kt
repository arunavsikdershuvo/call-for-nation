package com.mobioapp.callfornation.map.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WelfareList {
    @SerializedName("avg_family_wise_monthly_earning")
    var avgFamilyWiseMonthlyEarning: String? = null

    @SerializedName("avg_no_of_each_family_member")
    var avgNoOfEachFamilyMember: String? = null

    @Expose
    var id: String? = null

    @SerializedName("name_bn")
    var nameBn: String? = null

    @SerializedName("no_of_families")
    var noOfFamilies: String? = null

    @SerializedName("no_of_poor_family")
    var noOfPoorFamily: String? = null

    @SerializedName("no_of_population")
    var noOfPopulation: String? = null

    @SerializedName("total_survival_family_till_today")
    var totalSurvivalFamilyTillToday: Long? = null

    @SerializedName("upazila_id")
    var upazilaId: String? = null

    @SerializedName("upazila_latitude")
    var upazilaLatitude: String? = null

    @SerializedName("upazila_longitude")
    var upazilaLongitude: String? = null

    @SerializedName("upazila_name")
    var upazilaName: String? = null

    @SerializedName("upazila_wise_map_status")
    var upazilaWiseMapStatus: Int? = null

    @SerializedName("welfare_org_id")
    var welfareOrgId: String? = null


}