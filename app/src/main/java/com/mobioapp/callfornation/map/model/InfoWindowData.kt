package com.mobioapp.callfornation.map.model

import android.text.Spannable

data class InfoWindowData(val title: String, val info: String, val popUpData: Spannable)