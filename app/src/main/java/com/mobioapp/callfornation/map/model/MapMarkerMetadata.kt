package com.mobioapp.callfornation.map.model

data class MapMarkerMetadata(val lat: Double, val lon: Double, val upozillaId: String)