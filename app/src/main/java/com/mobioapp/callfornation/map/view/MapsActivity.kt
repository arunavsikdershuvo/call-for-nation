package com.mobioapp.callfornation.map.view

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.mobioapp.callfornation.ProgressDialog
import com.mobioapp.callfornation.R
import com.mobioapp.callfornation.map.model.InfoWindowData
import com.mobioapp.callfornation.map.model.MapCircleMetadata
import com.mobioapp.callfornation.map.model.MapMarkerMetadata
import com.mobioapp.callfornation.map.viewmodel.MapViewModel


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private val viewModel by lazy { ViewModelProviders.of(this)[MapViewModel::class.java] }
    private val progressDialog by lazy { ProgressDialog(this) }
    private val infoWindowMap = HashMap<String, InfoWindowData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        progressDialog.show()
        viewModel.fetchMapData()
        viewModel.fetchDistributionData()
    }

    private fun onMapCircleDataReceived(list: List<MapCircleMetadata>?) {
        progressDialog.dismiss()
        if (list == null || list.isEmpty()) return
        list.forEach {
            val (lat, lon, color, info) = it
            val position = it.toLatLng()
            val circleOptions = CircleOptions()
                    .center(position)
                    .radius(1000.0 * 5)// 10 KM Radius
                    .fillColor(color)
            mMap.addCircle(circleOptions)
        }

        mMap.zoomTo(list[0], 9f)

    }

    private fun onMapMarkerDataReceived(list: List<MapMarkerMetadata>?) {
        if (list == null || list.isEmpty()) return
        list.forEach {
            it.toLatLng()
            val marker = mMap.addMarker(MarkerOptions()
                    .position(it.toLatLng())
            )
            marker.tag = it.upozillaId
            marker.title = "Loading please wait..."
            marker.snippet = "Loading"
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoContents(marker: Marker): View {
                val info = LinearLayout(this@MapsActivity)
                info.orientation = LinearLayout.VERTICAL

                val title = TextView(this@MapsActivity)
                title.setTextColor(Color.BLACK)
                title.gravity = Gravity.CENTER
                title.setTypeface(null, Typeface.BOLD)
                title.text = marker.title

                val snippet = TextView(this@MapsActivity)
                snippet.setTextColor(Color.GRAY)
                snippet.text = marker.snippet

                info.addView(title)
                info.addView(snippet)

                return info
            }

            override fun getInfoWindow(p0: Marker?): View? {
                return null
            }

        })

        mMap.setOnMarkerClickListener { marker ->
            viewModel.fetchUpozilaSpecificData(marker.tag.toString()).observe(this, Observer {
                val title = it?.title ?: "No Data Found"
                val info = it?.info ?: "No Data Found"
                marker.title = title
                marker.snippet = info
                marker.showInfoWindow()
                if (it != null)
                    infoWindowMap.put(marker.tag.toString(), it)
            })
            return@setOnMarkerClickListener false
        }

        mMap.setOnInfoWindowClickListener { marker ->
            val key = marker.tag.toString()
            infoWindowMap[key]?.apply {
                AlertDialog.Builder(this@MapsActivity)
                        .setTitle(marker.title)
                        .setMessage(popUpData)
                        .show()
            }

        }

        viewModel.mapCircleDataLD.observe(this, Observer { onMapCircleDataReceived(it) })
        viewModel.mapMarkerDataLD.observe(this, Observer { onMapMarkerDataReceived(it) })
    }

    private fun Int.toBitmapDescriptor(): BitmapDescriptor? {
        val hsv = FloatArray(3)
        Color.colorToHSV(this, hsv)
        return BitmapDescriptorFactory.defaultMarker(hsv[0])
    }

    private fun MapCircleMetadata.toLatLng(): LatLng = LatLng(lat, lon)

    private fun GoogleMap.zoomTo(mapCircleMetadata: MapCircleMetadata, zoomAmount: Float = 15f) {
        moveCamera(CameraUpdateFactory.newLatLngZoom(mapCircleMetadata.toLatLng(), zoomAmount))

        animateCamera(CameraUpdateFactory.zoomIn())
        animateCamera(CameraUpdateFactory.zoomTo(zoomAmount), 2000, null);
    }

    private fun MapMarkerMetadata.toLatLng() = LatLng(lat, lon)
}








