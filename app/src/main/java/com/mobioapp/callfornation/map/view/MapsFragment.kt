package com.mobioapp.callfornation.map.view

import android.Manifest
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mobioapp.callfornation.ProgressDialog
import com.mobioapp.callfornation.map.model.InfoWindowData
import com.mobioapp.callfornation.map.model.MapCircleMetadata
import com.mobioapp.callfornation.map.model.MapMarkerMetadata
import com.mobioapp.callfornation.map.viewmodel.MapViewModel
import com.mobioapp.callfornation.utils.CurrentLocationFetcher


class MapsFragment : SupportMapFragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private val viewModel by lazy { ViewModelProviders.of(this)[MapViewModel::class.java] }
    private val progressDialog by lazy { ProgressDialog(context!!) }
    private val infoWindowMap = HashMap<String, InfoWindowData>()
    private val currentLocationFetcher by lazy { activity?.let { CurrentLocationFetcher(it) } }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getMapAsync(this)

        progressDialog.show()
        viewModel.fetchMapData()
        viewModel.fetchDistributionData()
        initLocation()
    }


    fun initLocation() {
        Dexter.withContext(activity)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            val successful = currentLocationFetcher?.fetchOnce()
                            Log.d("LocationFetch", successful.toString() + "")
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {}
                }).check()
    }

    private fun onMapCircleDataReceived(list: List<MapCircleMetadata>?) {
        progressDialog.dismiss()
        if (list == null || list.isEmpty()) return
        list.forEach {
            val (lat, lon, color, info) = it
            val position = it.toLatLng()
            val circleOptions = CircleOptions()
                    .center(position)
                    .radius(1000.0 * 10)// 10 KM Radius
                    .fillColor(color)
            mMap.addCircle(circleOptions)
        }

    }

    private fun onMapMarkerDataReceived(list: List<MapMarkerMetadata>?) {
        if (list == null || list.isEmpty()) return
        list.forEach {
            it.toLatLng()
            val marker = mMap.addMarker(MarkerOptions()
                    .position(it.toLatLng())
            )
            marker.tag = it.upozillaId
            marker.title = "Loading please wait..."
            marker.snippet = "Loading"
        }

    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoContents(marker: Marker): View {
                val info = LinearLayout(context)
                info.orientation = LinearLayout.VERTICAL

                val title = TextView(context)
                title.setTextColor(Color.BLACK)
                title.gravity = Gravity.CENTER
                title.setTypeface(null, Typeface.BOLD)
                title.text = marker.title

                val snippet = TextView(context)
                snippet.setTextColor(Color.GRAY)
                snippet.text = marker.snippet

                info.addView(title)
                info.addView(snippet)

                return info
            }

            override fun getInfoWindow(p0: Marker?): View? {
                return null
            }

        })

        mMap.setOnMarkerClickListener { marker ->
            viewModel.fetchUpozilaSpecificData(marker.tag.toString()).observe(this, Observer {
                val title = it?.title ?: "No Data Found"
                val info = it?.info ?: "No Data Found"
                marker.title = title
                marker.snippet = info
                marker.showInfoWindow()
                if (it != null)
                    infoWindowMap.put(marker.tag.toString(), it)
            })
            return@setOnMarkerClickListener false
        }

        mMap.setOnInfoWindowClickListener { marker ->
            val key = marker.tag.toString()
            infoWindowMap[key]?.apply {
                AlertDialog.Builder(context ?: return@setOnInfoWindowClickListener)
                        .setTitle(marker.title)
                        .setMessage(popUpData)
                        .show()
            }

        }

        viewModel.mapCircleDataLD.observe(viewLifecycleOwner, Observer {
            onMapCircleDataReceived(it)
        })
        viewModel.mapMarkerDataLD.observe(viewLifecycleOwner, Observer { onMapMarkerDataReceived(it) })

        currentLocationFetcher?.locationLiveData?.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                mMap.isMyLocationEnabled = true
                val cameraPosition = CameraPosition.builder()
                        .target(it)
                        .zoom(18f)
                        .bearing(0f)
                        .tilt(45f)
                        .build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000, null)
            }
        })
    }


    private fun MapCircleMetadata.toLatLng(): LatLng = LatLng(lat, lon)

    private fun GoogleMap.zoomTo(latLng: LatLng, zoomAmount: Float = 10f) {
        val cameraPosition = CameraPosition.builder()
                .target(latLng)
                .zoom(zoomAmount)
                .bearing(0f)
                .tilt(45f)
                .build()


        animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000, null)
    }

    private fun GoogleMap.zoomTo(mapCircleMetadata: MapCircleMetadata, zoomAmount: Float = 10f) {

        val cameraPosition = CameraPosition.builder()
                .target(mapCircleMetadata.toLatLng())
                .zoom(zoomAmount)
                .bearing(0f)
                .tilt(45f)
                .build()


        animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000, null)
    }

    private fun MapMarkerMetadata.toLatLng() = LatLng(lat, lon)
}
