package com.mobioapp.callfornation.map.model

import com.google.android.gms.maps.model.LatLng

data class MapCircleMetadata(val lat: Double, val lon: Double, val color: Int, val info: String) {
    var polyCoordinates: List<LatLng>? = null
}