
package com.mobioapp.callfornation.map.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class DistributionByUpozillaDto {

    @SerializedName("distributed_list")
    private List<DistributedList> distributedList;
    @Expose
    private String msg;
    @Expose
    private String status;
    @SerializedName("to_be_distributed")
    private List<ToBeDistributed> toBeDistributed;
    @SerializedName("upazila_detials")
    private List<UpazilaDetial> upazilaDetials;

    public List<DistributedList> getDistributedList() {
        return distributedList;
    }

    public void setDistributedList(List<DistributedList> distributedList) {
        this.distributedList = distributedList;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ToBeDistributed> getToBeDistributed() {
        return toBeDistributed;
    }

    public void setToBeDistributed(List<ToBeDistributed> toBeDistributed) {
        this.toBeDistributed = toBeDistributed;
    }

    public List<UpazilaDetial> getUpazilaDetials() {
        return upazilaDetials;
    }

    public void setUpazilaDetials(List<UpazilaDetial> upazilaDetials) {
        this.upazilaDetials = upazilaDetials;
    }

}
