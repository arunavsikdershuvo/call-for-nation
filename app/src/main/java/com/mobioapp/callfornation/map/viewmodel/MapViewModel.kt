package com.mobioapp.callfornation.map.viewmodel

import android.app.Application
import android.location.Geocoder
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.core.text.bold
import androidx.core.text.underline
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.mobioapp.callfornation.R
import com.mobioapp.callfornation.map.dto.DistributionByUpozillaDto
import com.mobioapp.callfornation.map.dto.WelfareList
import com.mobioapp.callfornation.map.model.InfoWindowData
import com.mobioapp.callfornation.map.model.MapCircleMetadata
import com.mobioapp.callfornation.map.model.MapMarkerMetadata
import com.mobioapp.callfornation.map.network.MapNetworkCalls
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class MapViewModel(private val app: Application) : AndroidViewModel(app) {

    private val mapNetworkCalls by lazy { MapNetworkCalls() }
    val mapCircleDataLD = MutableLiveData<List<MapCircleMetadata>>()
    val mapMarkerDataLD = MutableLiveData<List<MapMarkerMetadata>>()

    private val geocoder by lazy { Geocoder(app) }

    private val LOGCAT = javaClass.simpleName

    fun fetchMapData() = viewModelScope.async(Dispatchers.IO) {

        val welfareList = mapNetworkCalls.getWelfareData()?.body()?.welfareList
                ?: emptyList<WelfareList>()
        val mapMetadatas = welfareList.map { welfareListData ->
            val mapMetadatas = welfareListData.run {
                val color = when (this.upazilaWiseMapStatus) {
                    1 -> R.color.poor
                    2 -> R.color.rich
                    3 -> R.color.middle_class
                    else -> R.color.middle_class;
                }

                return@run MapCircleMetadata(
                        this.upazilaLatitude?.toDouble() ?: 0.0,
                        this.upazilaLongitude?.toDouble() ?: 0.0,
                        ContextCompat.getColor(app, color),
                        "Average Family Members: $avgNoOfEachFamilyMember\n" +
                                "No of Population: $noOfPopulation\n" +
                                "No of Families: $noOfPopulation"
                )
            }
            mapMetadatas
        }

        mapCircleDataLD.postValue(mapMetadatas)
    }.apply {
        invokeOnCompletion {
            it?.run {
                printStackTrace()
                mapCircleDataLD.postValue(ArrayList())
            }
        }

    }


    fun fetchDistributionData() = viewModelScope.async(Dispatchers.IO) {
        val markerData = mapNetworkCalls.getDistributionData()?.body()?.distributionList?.map {
            MapMarkerMetadata(it.upazilaLatitude?.toDouble() ?: 0.0,
                    it.upazilaLongitude?.toDouble() ?: 0.0,
                    upozillaId = it.upazilaId ?: "0"
            )
        }
        mapMarkerDataLD.postValue(markerData)
    }.apply {
        invokeOnCompletion {
            it?.run {
                printStackTrace()
                mapMarkerDataLD.postValue(ArrayList())
            }
        }
    }


    fun fetchUpozilaSpecificData(upozillaId: String) = liveData {
        withContext(Dispatchers.IO)
        {
            val distributionData = mapNetworkCalls.getDistributionByUpozilla(upozillaId)?.body()?.run {

                InfoWindowData(this.upazilaDetials?.get(0)?.name
                        ?: "Upozilla Data", "Distributed : ${distributedList?.size ?: 0}\n" +
                        "To be Distributed : ${toBeDistributed?.size ?: 0}", this.toSpannable())
            }
            emit(distributionData)
        }
    }

    fun fetchAreaInfo(mapCircleMetadata: MapCircleMetadata) {
        val (lat, lon, color) = mapCircleMetadata
        val locationData = geocoder.getFromLocation(lat, lon, 1)[0]
        val latLng = geocoder.getFromLocationName(locationData.subAdminArea, 10)
                .map { LatLng(it.latitude, it.longitude) }
        Log.d(LOGCAT, locationData.toString())
    }


    private fun DistributionByUpozillaDto.toSpannable(): Spannable {
        return SpannableStringBuilder().apply {
            bold { underline { append("\n\nUpazilla Details:\n") } }
            append(upazilaDetials?.joinToString(separator = "\n\n") ?: "\n")
            bold { underline { append("\n\nDistributed:\n") } }
            append(distributedList?.joinToString(separator = "\n\n") ?: "\n")
            bold { underline { append("\n\nTo be Distributed:\n") } }
            append(toBeDistributed?.joinToString(separator = "\n\n") ?: "\n")
        }
    }
}


