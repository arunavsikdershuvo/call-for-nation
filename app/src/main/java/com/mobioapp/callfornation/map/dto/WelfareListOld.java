
package com.mobioapp.callfornation.map.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class WelfareListOld {

    @SerializedName("avg_no_of_each_family_member")
    private String avgNoOfEachFamilyMember;
    @Expose
    private String id;
    @SerializedName("is_poor")
    private String isPoor;
    @SerializedName("no_of_families")
    private String noOfFamilies;
    @SerializedName("no_of_poor_people")
    private String noOfPoorPeople;
    @SerializedName("no_of_population")
    private String noOfPopulation;
    @SerializedName("upazila_id")
    private String upazilaId;
    @SerializedName("upazila_latitude")
    private String upazilaLatitude;
    @SerializedName("upazila_longitude")
    private String upazilaLongitude;
    @SerializedName("upazila_name")
    private String upazilaName;
    @SerializedName("welfare_org_id")
    private String welfareOrgId;

    public String getAvgNoOfEachFamilyMember() {
        return avgNoOfEachFamilyMember;
    }

    public void setAvgNoOfEachFamilyMember(String avgNoOfEachFamilyMember) {
        this.avgNoOfEachFamilyMember = avgNoOfEachFamilyMember;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsPoor() {
        return isPoor;
    }

    public void setIsPoor(String isPoor) {
        this.isPoor = isPoor;
    }

    public String getNoOfFamilies() {
        return noOfFamilies;
    }

    public void setNoOfFamilies(String noOfFamilies) {
        this.noOfFamilies = noOfFamilies;
    }

    public String getNoOfPoorPeople() {
        return noOfPoorPeople;
    }

    public void setNoOfPoorPeople(String noOfPoorPeople) {
        this.noOfPoorPeople = noOfPoorPeople;
    }

    public String getNoOfPopulation() {
        return noOfPopulation;
    }

    public void setNoOfPopulation(String noOfPopulation) {
        this.noOfPopulation = noOfPopulation;
    }

    public String getUpazilaId() {
        return upazilaId;
    }

    public void setUpazilaId(String upazilaId) {
        this.upazilaId = upazilaId;
    }

    public String getUpazilaLatitude() {
        return upazilaLatitude;
    }

    public void setUpazilaLatitude(String upazilaLatitude) {
        this.upazilaLatitude = upazilaLatitude;
    }

    public String getUpazilaLongitude() {
        return upazilaLongitude;
    }

    public void setUpazilaLongitude(String upazilaLongitude) {
        this.upazilaLongitude = upazilaLongitude;
    }

    public String getUpazilaName() {
        return upazilaName;
    }

    public void setUpazilaName(String upazilaName) {
        this.upazilaName = upazilaName;
    }

    public String getWelfareOrgId() {
        return welfareOrgId;
    }

    public void setWelfareOrgId(String welfareOrgId) {
        this.welfareOrgId = welfareOrgId;
    }

}
