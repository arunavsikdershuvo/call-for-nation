package com.mobioapp.callfornation.map.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DistributionDto {
    @SerializedName("distribution_list")
    var distributionList: List<DistributionList>? = null

    @Expose
    var msg: String? = null

    @Expose
    var status: String? = null

}