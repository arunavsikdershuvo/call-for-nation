
package com.mobioapp.callfornation.map.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UpazilaDetial {

    @SerializedName("avg_family_wise_monthly_earning")
    private String avgFamilyWiseMonthlyEarning;
    @SerializedName("avg_no_of_each_family_member")
    private String avgNoOfEachFamilyMember;
    @Expose
    private String latitude;
    @Expose
    private String longitude;
    @Expose
    private String name;
    @SerializedName("no_of_families")
    private String noOfFamilies;
    @SerializedName("no_of_population")
    private String noOfPopulation;

    public String getAvgFamilyWiseMonthlyEarning() {
        return avgFamilyWiseMonthlyEarning;
    }

    public void setAvgFamilyWiseMonthlyEarning(String avgFamilyWiseMonthlyEarning) {
        this.avgFamilyWiseMonthlyEarning = avgFamilyWiseMonthlyEarning;
    }

    public String getAvgNoOfEachFamilyMember() {
        return avgNoOfEachFamilyMember;
    }

    public void setAvgNoOfEachFamilyMember(String avgNoOfEachFamilyMember) {
        this.avgNoOfEachFamilyMember = avgNoOfEachFamilyMember;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoOfFamilies() {
        return noOfFamilies;
    }

    public void setNoOfFamilies(String noOfFamilies) {
        this.noOfFamilies = noOfFamilies;
    }

    public String getNoOfPopulation() {
        return noOfPopulation;
    }

    public void setNoOfPopulation(String noOfPopulation) {
        this.noOfPopulation = noOfPopulation;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("");
        sb.append("Family wiseMonthly Earning(avg)=").append(avgFamilyWiseMonthlyEarning).append('\n');
        sb.append("Each Family Member Number(avg)=").append(avgNoOfEachFamilyMember).append('\n');
//        sb.append("latitude=").append(latitude).append('\n');
//        sb.append("longitude=").append(longitude).append('\n');
        sb.append("Name=").append(name).append('\n');
        sb.append("Families Count=").append(noOfFamilies).append('\n');
        sb.append("Population=").append(noOfPopulation).append('\n');
        return sb.toString();
    }
}
