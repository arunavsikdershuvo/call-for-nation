package com.mobioapp.callfornation.map.network

import retrofit2.Retrofit

class MapNetworkCalls
{
    private val retrofit: Retrofit? = RetrofitClient.retrofitInstance
    private val mapApi by lazy { retrofit?.create(MapApi::class.java) }

    fun getWelfareData () = try {
        val welfareList = mapApi?.getWelfareData()?.execute()
        welfareList
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }

    fun getDistributionData() = try {
        mapApi?.getDistributionData()?.execute()
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }

    fun getDistributionByUpozilla(upozillaId: String) = try {
        mapApi?.getDistributionByUpozilla(upozillaId)?.execute()
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }

}