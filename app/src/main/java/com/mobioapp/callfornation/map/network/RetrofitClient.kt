package com.mobioapp.callfornation.map.network

import com.mobioapp.callfornation.configuration.AppConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    private var retrofit: Retrofit? = null

    val retrofitInstance: Retrofit?
        get() {
            if (retrofit == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

                val client = OkHttpClient.Builder()
                        .addInterceptor(interceptor)
//                        .addInterceptor(NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                        .build()

                retrofit = Retrofit.Builder()
                        .baseUrl(AppConfig.getBaseURL())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build()
            }
            return retrofit
        }
}