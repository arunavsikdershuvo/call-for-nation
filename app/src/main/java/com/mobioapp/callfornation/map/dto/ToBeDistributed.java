
package com.mobioapp.callfornation.map.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ToBeDistributed {

    @Expose
    private String address;
    @SerializedName("date_of_distribution")
    private String dateOfDistribution;
    @SerializedName("is_needed")
    private String isNeeded;
    @SerializedName("is_needed_detials")
    private String isNeededDetials;
    @SerializedName("no_of_family")
    private String noOfFamily;
    @SerializedName("releife_items")
    private String releifeItems;
    @SerializedName("survival_day")
    private String survivalDay;
    @SerializedName("upazila_id")
    private String upazilaId;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("user_name")
    private String userName;
    @SerializedName("user_type")
    private String userType;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDateOfDistribution() {
        return dateOfDistribution;
    }

    public void setDateOfDistribution(String dateOfDistribution) {
        this.dateOfDistribution = dateOfDistribution;
    }

    public String getIsNeeded() {
        return isNeeded;
    }

    public void setIsNeeded(String isNeeded) {
        this.isNeeded = isNeeded;
    }

    public String getIsNeededDetials() {
        return isNeededDetials;
    }

    public void setIsNeededDetials(String isNeededDetials) {
        this.isNeededDetials = isNeededDetials;
    }

    public String getNoOfFamily() {
        return noOfFamily;
    }

    public void setNoOfFamily(String noOfFamily) {
        this.noOfFamily = noOfFamily;
    }

    public String getReleifeItems() {
        return releifeItems;
    }

    public void setReleifeItems(String releifeItems) {
        this.releifeItems = releifeItems;
    }

    public String getSurvivalDay() {
        return survivalDay;
    }

    public void setSurvivalDay(String survivalDay) {
        this.survivalDay = survivalDay;
    }

    public String getUpazilaId() {
        return upazilaId;
    }

    public void setUpazilaId(String upazilaId) {
        this.upazilaId = upazilaId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Address=").append(address).append("\n");
        sb.append("DateOfDistribution=").append(dateOfDistribution).append("\n");
        sb.append("Needed=").append(isNeeded).append("\n");
        sb.append("Details Needed=").append(isNeededDetials).append("\n");
        sb.append("Family Count=").append(noOfFamily).append("\n");
        sb.append("Relief Items=").append(releifeItems).append("\n");
        sb.append("Survival Day=").append(survivalDay).append("\n");
        sb.append("Upozilla Id=").append(upazilaId).append("\n");
        sb.append("User Id=").append(userId).append("\n");
        sb.append("User Name=").append(userName).append("\n");
        sb.append("User Type=").append(userType).append("\n");
        return sb.toString();
    }
}
