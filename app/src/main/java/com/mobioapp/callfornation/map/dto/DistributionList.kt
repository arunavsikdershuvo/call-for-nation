package com.mobioapp.callfornation.map.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DistributionList {
    @Expose
    var address: String? = null

    @SerializedName("date_of_distribution")
    var dateOfDistribution: Any? = null

    @SerializedName("is_needed")
    var isNeeded: Any? = null

    @SerializedName("is_needed_detials")
    var isNeededDetials: Any? = null

    @SerializedName("no_of_family")
    var noOfFamily: String? = null

    @SerializedName("releife_items")
    var releifeItems: Any? = null

    @SerializedName("survival_day")
    var survivalDay: Any? = null

    @SerializedName("upazila_id")
    var upazilaId: String? = null

    @SerializedName("upazila_latitude")
    var upazilaLatitude: String? = null

    @SerializedName("upazila_longitude")
    var upazilaLongitude: String? = null

    @SerializedName("upazila_name")
    var upazilaName: String? = null

    @SerializedName("user_id")
    var userId: String? = null

    @SerializedName("user_name")
    var userName: String? = null

    @SerializedName("user_type")
    var userType: String? = null

}