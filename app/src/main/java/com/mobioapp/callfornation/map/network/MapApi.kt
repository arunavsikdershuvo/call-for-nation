package com.mobioapp.callfornation.map.network

import com.mobioapp.callfornation.map.dto.DistributionByUpozillaDto
import com.mobioapp.callfornation.map.dto.DistributionDto
import com.mobioapp.callfornation.map.dto.WelfareData
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface MapApi {
    @GET("get_welfare_data.php")
    fun getWelfareData():Call<WelfareData>

    @POST("distribution_list.php")
    fun getDistributionData(): Call<DistributionDto>

    @FormUrlEncoded
    @POST("distribution_by_upazila.php")
    fun getDistributionByUpozilla(@Field("upazila_id") upozilaId: String): Call<DistributionByUpozillaDto>
}