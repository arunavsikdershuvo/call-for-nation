
package com.mobioapp.callfornation.map.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class WelfareData {

    @Expose
    private String msg;
    @Expose
    private String status;
    @SerializedName("welfare_list")
    private List<WelfareList> welfareList;
    @SerializedName("welfare_list_old")
    private List<WelfareListOld> welfareListOld;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<WelfareList> getWelfareList() {
        return welfareList;
    }

    public void setWelfareList(List<WelfareList> welfareList) {
        this.welfareList = welfareList;
    }

    public List<WelfareListOld> getWelfareListOld() {
        return welfareListOld;
    }

    public void setWelfareListOld(List<WelfareListOld> welfareListOld) {
        this.welfareListOld = welfareListOld;
    }

}
