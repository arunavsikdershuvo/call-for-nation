package com.mobioapp.callfornation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.mobioapp.callfornation.controller.RegistrationController;
import com.mobioapp.callfornation.database.PreferenceUtil;
import com.mobioapp.callfornation.listener.RegistrationListener;
import com.mobioapp.callfornation.model.LoginInfo;
import com.mobioapp.callfornation.model.RegistrationInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;
import com.mobioapp.callfornation.utils.ValidationUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mobioapp.callfornation.utils.Utils.isOnline;

public class RegistrationActivity extends AppCompatActivity implements RegistrationListener {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.linearLayoutLogin)
    LinearLayout linearLayoutLogin;

    @BindView(R.id.linearLayoutPhoneWarning)
    LinearLayout linearLayoutPhoneWarning;

    @BindView(R.id.spinner_organisation)
    Spinner spinnerOrganisationType;

    @BindView(R.id.etUserName)
    EditText editTextUserName;

    @BindView(R.id.etName)
    EditText editTextFullName;

    @BindView(R.id.etEmail)
    EditText editTextEmail;

    @BindView(R.id.etPhoneNumber)
    EditText editTextPhoneNumber;

    @BindView(R.id.etPassword)
    EditText editTextPassword;

    @BindView(R.id.etConfirmPassword)
    EditText editTextConfirmPassword;

    @BindView(R.id.layoutTextInputEmailLayout)
    TextInputLayout textInputLayoutEmail;

    @BindView(R.id.layoutTextInputPhoneNumberLayout)
    TextInputLayout textInputLayoutPhone;

    @BindView(R.id.btnSignUp)
    Button buttonSinUp;

    private Activity activity;
    private ValidationUtils utils;
    private RegistrationController controller;
    private PreferenceUtil preferenceUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        initViews();

    }

    private void initViews() {

        activity = RegistrationActivity.this;
        utils = new ValidationUtils(activity);
        preferenceUtil = new PreferenceUtil(activity);
    }


    @OnClick(R.id.linearLayoutLogin)
    void goToLogin() {
        startActivity(new Intent(activity, LogInActivity.class));
        finish();
    }

    @OnClick(R.id.btnSignUp)
    void processRegistration() {

        if (!isOnline(activity)) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
            return;
        }

        resetFieldsWarnings();

        if (spinnerOrganisationType.getSelectedItemPosition() <= 0) {
            new MyCustomToast(CFNApplication.getInstance()).showToast(getString(R.string.please_select_org_type));
            return;
        }

        //at first check mandatory fields are empty
        if (spinnerOrganisationType.getSelectedItemPosition() <= 0 ||
                editTextUserName.getText().length() <= 0 ||
                editTextPhoneNumber.getText().length() <= 0 ||
                editTextPassword.getText().length() <= 0) {
            new MyCustomToast(CFNApplication.getInstance()).showToast(getString(R.string.fill_up_mandatory_fields));
            return;
        }

        if (editTextEmail.getText().length() > 0) {
            if (!utils.isValidEmail(editTextEmail.getText().toString().trim())) {
                new MyCustomToast(CFNApplication.getInstance()).showToast(getString(R.string.please_provide_valid_email));
                textInputLayoutEmail.setError("Email is not valid");
                return;
            }
        }

        if (editTextPhoneNumber.getText().toString().trim().length() < 10 || !utils.isPhoneNumberValid("+880", editTextPhoneNumber.getText().toString().trim())) {
                new MyCustomToast(CFNApplication.getInstance()).showToast(getString(R.string.please_provide_valid_phone));
                linearLayoutPhoneWarning.setVisibility(View.VISIBLE);
                return;
        }

        if (editTextConfirmPassword.getText().length() > 0) {
            if (!editTextPassword.getText().toString().trim().equals(editTextConfirmPassword.getText().toString().trim())) {
                new MyCustomToast(CFNApplication.getInstance()).showToast(getString(R.string.password_does_not_match));
                return;
            }
        } else {
            new MyCustomToast(CFNApplication.getInstance()).showToast(getString(R.string.confirm_pass_fied_cannot_empty));
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        buttonSinUp.setEnabled(false);

        String phoneNumber = "0" + editTextPhoneNumber.getText().toString().trim();
        controller = new RegistrationController(this);
        controller.start(editTextFullName.getText().toString().trim(),
                editTextUserName.getText().toString().trim(),
                editTextPassword.getText().toString().trim(),
                phoneNumber,
                editTextEmail.getText().toString().trim(),
                "",
                String.valueOf(spinnerOrganisationType.getSelectedItemPosition()));

    }

    private void resetFieldsWarnings() {

        linearLayoutPhoneWarning.setVisibility(View.GONE);
        textInputLayoutEmail.setError(null);

    }

    @Override
    public void registrationCompleted(RegistrationInfo registrationInfo) {

        progressBar.setVisibility(View.GONE);
        buttonSinUp.setEnabled(true);

        if (registrationInfo != null) {
            preferenceUtil.setLogin(false);
            preferenceUtil.updateLoginData(null);
            goToLoginScreen();
        } else {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.there_is_a_problem));
        }

    }

    @Override
    public void registrationFailed(String message) {
        progressBar.setVisibility(View.GONE);
        buttonSinUp.setEnabled(true);
        new MyCustomToast(getApplicationContext()).showToast(getResources().getString(R.string.failed_to_signup));
    }

    private void goToLoginScreen() {

        Intent intent = new Intent(activity, LogInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (controller != null)
            controller.removeListener();
    }
}
