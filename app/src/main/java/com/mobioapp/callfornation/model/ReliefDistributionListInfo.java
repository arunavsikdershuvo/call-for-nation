package com.mobioapp.callfornation.model;

import java.util.List;

import lombok.Data;

@Data
public class ReliefDistributionListInfo {

    String status;
    String msg;
    List<ReliefDistribution> distributed_list;
    List<ReliefDistribution> distribution_list;

}
