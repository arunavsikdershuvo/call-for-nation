package com.mobioapp.callfornation.model;

import lombok.Data;

@Data
public class DashboardScore {

    String total_user;
    String total_private_welfare;
    String total_govt_organization;
    String total_distributed_pending;
    String total_distributed_done;
    String total_distributed_cancel;
    String total_relief_request;
    String total_feedback;
    int total_poor_family;
    int total_family_food_have;
    int total_family_food_needed;

}
