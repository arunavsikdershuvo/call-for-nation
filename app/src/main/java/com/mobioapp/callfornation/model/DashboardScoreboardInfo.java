package com.mobioapp.callfornation.model;

import java.util.List;

import lombok.Data;

@Data
public class DashboardScoreboardInfo {

    List<DashboardScore> dashboard;

}
