package com.mobioapp.callfornation.model;

import lombok.Data;

@Data
public class LoginInfo {

    public String status;
    public String msg;
    public String type;
    public String name;
    public String phone;
    public String email;
    public String user_id;

}
