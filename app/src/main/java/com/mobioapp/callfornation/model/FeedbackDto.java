
package com.mobioapp.callfornation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class FeedbackDto {

    @Expose
    public String feedback;
    @SerializedName("upazila_id")
    public String upazilaId;
    @SerializedName("user_id")
    public String userId;

}
