package com.mobioapp.callfornation.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class ReliefDistribution implements Serializable {

    String id;
    public String no_of_family;
    public String user_name;
    public String user_type;
    public String releife_items;
    public String survival_day;
    public String is_needed;
    public String is_needed_detials;
    public String date_of_distribution; //format: 2020-04-22 11:22:43
    public String address;
    public String distribution_latitude;
    public String distributtion_longitude;
    public String upazila_id;
    public String upazila_name;
    public String upazila_name_bn;
    public String upazila_latitude;
    public String upazila_longitude;
    public String status;

}
