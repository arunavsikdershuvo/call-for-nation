package com.mobioapp.callfornation.model;

import java.util.List;

import lombok.Data;

@Data
public class ReliefRequestListInfo {

    String status;
    String msg;
    List<ReliefRequest> detials;

}
