package com.mobioapp.callfornation.model;

import java.util.List;

public class UpazillaInfo {

    public String status;
    public String msg;
    public List<Upazilla> upazila_list;

}
