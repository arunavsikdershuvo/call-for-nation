package com.mobioapp.callfornation.model;

import lombok.Data;

@Data
public class AddReliefRequestInfo {

    String status;
    String msg;

}
