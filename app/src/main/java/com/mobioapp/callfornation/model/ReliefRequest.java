package com.mobioapp.callfornation.model;

import lombok.Data;

@Data
public class ReliefRequest {

    String id;
    String user_id;
    String user_full_name;
    String upazila_name;
    String type;
    String name;
    String address;
    String no_of_family;
    String releife_items;
    String no_of_survival_day;
    String details;
    String nid;
    String status;
    String created_at;

}
