package com.mobioapp.callfornation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mobioapp.callfornation.adapter.TotalOrganisationAdapter;
import com.mobioapp.callfornation.controller.TotalOrganisationController;
import com.mobioapp.callfornation.listener.TotalOrganisationListener;
import com.mobioapp.callfornation.model.Organisation;
import com.mobioapp.callfornation.model.TotalOrganisationInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;
import com.mobioapp.callfornation.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mobioapp.callfornation.utils.Utils.isOnline;

public class TotalOrganisationFragment extends Fragment implements TotalOrganisationListener, TotalOrganisationAdapter.OnItemClickListener {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private String orgType = "";
    private Context context;

    private TotalOrganisationAdapter adapter;

    private List<Organisation> organisationList = null;

    public static TotalOrganisationFragment newInstance(String orgType) {
        TotalOrganisationFragment fragment = new TotalOrganisationFragment();
        fragment.setOrgType(orgType);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_total_org, container, false);
        ButterKnife.bind(this, fragmentView);

        initViews();
        setListener();
        loadData();

        return fragmentView;
    }

    private void loadData() {

        if (!isOnline(context)) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        TotalOrganisationController controller = new TotalOrganisationController(this);
        controller.start(orgType);
    }

    private void setListener() {



    }

    private void initViews() {

        context = getActivity();

        if (organisationList == null)
            organisationList = new ArrayList<>();

        adapter = new TotalOrganisationAdapter(context, this, organisationList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration dividerItemDecoration = new Utils.DividerItemDecorator(ContextCompat.getDrawable(context, R.drawable.my_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void loadTotalOrganisationCompleted(TotalOrganisationInfo totalOrganisationInfo) {

        progressBar.setVisibility(View.GONE);

        if (totalOrganisationInfo != null) {
            organisationList = new ArrayList<>();
            this.organisationList = totalOrganisationInfo.users_list;
            if (organisationList != null && organisationList.size() > 0) {
                updateTabCount();
                adapter.updateData(organisationList);
                adapter.notifyDataSetChanged();
            }
        }

    }

    private void updateTabCount() {

        if (((TotalOrganisationActivity) getActivity()).tabs != null && ((TotalOrganisationActivity) getActivity()).tabs.getTabCount() > 1) {
            if (orgType.equals("1")) {
                if (((TotalOrganisationActivity) getActivity()).tabs.getTabAt(2) != null) {
                    ((TotalOrganisationActivity) getActivity()).tabs.getTabAt(2).setText(getResources().getString(R.string.individual) + "\n(" + String.format("%02d", organisationList.size()) + ")");
                }
            } else if (orgType.equals("2")) {
                if (((TotalOrganisationActivity) getActivity()).tabs.getTabAt(1) != null) {
                    ((TotalOrganisationActivity) getActivity()).tabs.getTabAt(1).setText(getResources().getString(R.string.private_org) + "\n(" + String.format("%02d", organisationList.size()) + ")");
                }
            } else if (orgType.equals("3")) {
                if (((TotalOrganisationActivity) getActivity()).tabs.getTabAt(0) != null) {
                    ((TotalOrganisationActivity) getActivity()).tabs.getTabAt(0).setText(getResources().getString(R.string.govt_org) + "\n(" + String.format("%02d", organisationList.size()) + ")");
                }
            }
        }

    }

    @Override
    public void loadTotalOrganisationFailed(String message) {
        progressBar.setVisibility(View.GONE);
    }

    private void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    @Override
    public void onItemClick(View View, int position, Organisation organisation) {
        new MyCustomToast(CFNApplication.getAppContext()).showToast("Under Development!");
    }
}
