package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.TotalOrganisationInfo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface TotalOrganisationApi {

    @FormUrlEncoded
    @POST("type_wise_user_list.php")
    Call<TotalOrganisationInfo> loadTotalOrgData(@Field("user_type") String user_type);

}
