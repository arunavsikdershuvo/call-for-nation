package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.UpazillaInfo;

public interface AddUpazillaListListener {

        void addUpazillaListCompleted(UpazillaInfo upazillaInfo);

        void addUpazillaListFailed(String message);

}
