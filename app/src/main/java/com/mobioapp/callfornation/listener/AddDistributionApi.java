package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.AddDistributionInfo;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface AddDistributionApi {

    @FormUrlEncoded
    @POST("add_distribute.php")
    Call<AddDistributionInfo> addDistribution(@Field("user_id") String user_id,
                                              @Field("upazila_id") String upazila_id,
                                              @Field("no_of_family") Integer no_of_family,
                                              @Field("releife_items") String releife_items,
                                              @Field("survival_day") String survival_day,
                                              @Field("is_needed") Integer is_needed,
                                              @Field("is_needed_detials") String is_needed_detials,
                                              @Field("date_of_distribution") String date_of_distribution,
                                              @Field("address") String address,
                                              @Field("latitude") String latitude,
                                              @Field("longitude") String longitude,
                                              @Field("photo") String photo);

    @Multipart
    @POST("update_distribute.php")
    Call<AddDistributionInfo> updateDistribution(@Part("user_id") RequestBody user_id,
                                              @Part("upazila_id") RequestBody upazila_id,
                                              @Part("no_of_family") RequestBody no_of_family,
                                              @Part("releife_items") RequestBody releife_items,
                                              @Part("survival_day") RequestBody survival_day,
                                              @Part("is_needed") RequestBody is_needed,
                                              @Part("is_needed_detials") RequestBody is_needed_detials,
                                              @Part("date_of_distribution") RequestBody date_of_distribution,
                                              @Part("address") RequestBody address,
                                              @Part("latitude") RequestBody latitude,
                                              @Part("longitude") RequestBody longitude,
                                              @Part("status") RequestBody status,
                                              @Part("id") RequestBody id, @Part MultipartBody.Part photo_id_back);

}
