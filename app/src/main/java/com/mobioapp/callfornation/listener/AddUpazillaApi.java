package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.UpazillaInfo;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AddUpazillaApi {

    @GET("upazila_list.php")
    Call<UpazillaInfo> addDistribution();

}
