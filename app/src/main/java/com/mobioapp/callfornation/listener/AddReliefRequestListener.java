package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.AddReliefRequestInfo;

public interface AddReliefRequestListener {

    void addReliefRequestCompleted(AddReliefRequestInfo addReliefRequestInfo);

    void addReliefRequestFailed(String message);

}
