package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.ReliefRequestListInfo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ReliefRequestListApi {

    @FormUrlEncoded
    @POST("relief_request_list.php")
    Call<ReliefRequestListInfo> getReliefRequestList(@Field("type") String type);

}