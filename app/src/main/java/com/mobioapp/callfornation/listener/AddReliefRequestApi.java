package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.AddReliefRequestInfo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AddReliefRequestApi {

    @FormUrlEncoded
    @POST("relief_request_add.php")
    Call<AddReliefRequestInfo> addReliefRequest(@Field("user_id") String user_id,
                                               @Field("upazila_id") String upazila_id,
                                               @Field("type") String type,
                                               @Field("name") String name,
                                               @Field("address") String address,
                                               @Field("no_of_family") String no_of_family,
                                               @Field("releife_items") String releife_items,
                                               @Field("no_of_survival_day") String no_of_survival_day,
                                               @Field("details") String details,
                                               @Field("nid") String nid);

}
