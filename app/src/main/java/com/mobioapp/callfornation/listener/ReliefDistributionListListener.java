package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.ReliefDistributionListInfo;

public interface ReliefDistributionListListener {

    void distributionInfoCompleted(ReliefDistributionListInfo reliefDistributionListInfo);

    void distributionInfoFailed(String message);

}
