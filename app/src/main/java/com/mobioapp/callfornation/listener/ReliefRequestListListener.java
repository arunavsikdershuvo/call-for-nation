package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.ReliefRequestListInfo;

public interface ReliefRequestListListener {

    void reliefRequestFetchCompleted(ReliefRequestListInfo reliefRequestListInfo);

    void reliefRequestFetchFailed(String message);

}
