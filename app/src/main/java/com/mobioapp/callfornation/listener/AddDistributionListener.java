package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.AddDistributionInfo;

public interface AddDistributionListener {

    void addDistributionCompleted(AddDistributionInfo addDistributionInfo);

    void addDistributionFailed(String message);

}
