package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.ReliefDistributionListInfo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ReliefDistributionListApi {

    @FormUrlEncoded
    @POST("distribution_by_user.php")
    Call<ReliefDistributionListInfo> getUserWiseReliefDistributionList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("distribution_list.php")
    Call<ReliefDistributionListInfo> getStatusWiseReliefDistributionList(@Field("status") String status);

}
