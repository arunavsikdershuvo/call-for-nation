package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.LoginInfo;

public interface LoginListener {

    void loginCompleted(LoginInfo loginInfo);

    void loginFailed(String message);

}
