package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.RegistrationInfo;

public interface RegistrationListener {

    void registrationCompleted(RegistrationInfo registrationInfo);

    void registrationFailed(String message);

}
