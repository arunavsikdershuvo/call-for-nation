package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.DashboardScoreboardInfo;
import com.mobioapp.callfornation.model.UpazillaInfo;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DashboardScoreApi {

    @GET("dashboard.php")
    Call<DashboardScoreboardInfo> loadDashboardScore();

}
