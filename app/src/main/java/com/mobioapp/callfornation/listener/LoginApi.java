package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.LoginInfo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginApi {

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginInfo> login(@Field("username") String userName, @Field("password") String password);

}
