package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.RegistrationInfo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegistrationApi {

    @FormUrlEncoded
    @POST("registration.php")
    Call<RegistrationInfo> register(@Field("name") String fullName,
                                    @Field("username") String userName,
                                    @Field("password") String password,
                                    @Field("phone") String phoneNumber,
                                    @Field("email") String email,
                                    @Field("address") String address,
                                    @Field("type") String type);

}
