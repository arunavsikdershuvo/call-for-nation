package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.DashboardScoreboardInfo;

public interface DashboardScoreboardListener{

    void loadScoreboardCompleted(DashboardScoreboardInfo dashboardScoreboardInfo);

    void loadScoreboardFailed(String message);

}
