package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.FeedbackDto;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface FeedbackApi {
    @POST("/cfn/add_feedback.php")
    public Call<ResponseBody> submitFeedback(@Body FeedbackDto feedbackDto);
}
