package com.mobioapp.callfornation.listener;

import com.mobioapp.callfornation.model.TotalOrganisationInfo;

public interface TotalOrganisationListener {

    void loadTotalOrganisationCompleted(TotalOrganisationInfo totalOrganisationInfo);

    void loadTotalOrganisationFailed(String message);

}
