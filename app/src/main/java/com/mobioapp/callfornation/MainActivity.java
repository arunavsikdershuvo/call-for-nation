package com.mobioapp.callfornation;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.mobioapp.callfornation.database.PreferenceUtil;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    NavController navController;

    @BindView(R.id.navigationView)
    NavigationView navigationView;

    private Activity activity;
    private PreferenceUtil preferenceUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupNavigation();
        initViews();
        setHeaderLayout();

    }

    private void setHeaderLayout() {
        View headerView = navigationView.getHeaderView(0);
        TextView textViewUserNameHeader = headerView.findViewById(R.id.textViewUserNameHeader);
        TextView textViewPhoneHeader = headerView.findViewById(R.id.textViewPhoneHeader);
        textViewUserNameHeader.setText(("").equals(preferenceUtil.retrieveLoginData().getName()) ? "Organisation/User Name" : preferenceUtil.retrieveLoginData().getName());
        String phone = preferenceUtil.retrieveLoginData().getPhone();
        if (phone != null)
            textViewPhoneHeader.setText(phone.isEmpty() ? "User Phone" : phone);

    }

    // Setting Up One Time Navigation
    private void setupNavigation() {

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.navigationView);
        navigationView.setItemIconTintList(null);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout);

        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.setNavigationItemSelectedListener(this);

    }

    private void initViews() {

        activity = MainActivity.this;
        setSupportActionBar(toolbar);
        preferenceUtil = new PreferenceUtil(activity);

    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(Navigation.findNavController(this, R.id.nav_host_fragment), drawerLayout);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        menuItem.setChecked(true);

        drawerLayout.closeDrawers();

        int id = menuItem.getItemId();

        switch (id) {

            case R.id.dashboard:
//                navController.navigate(R.id.FirstFragment);
                break;

            case R.id.map_view:
                navController.navigate(R.id.action_FirstFragment_to_mapsActivity);
                break;

            case R.id.distribute_relief:
                navController.navigate(R.id.action_FirstFragment_to_distributionActivity);
                break;

            case R.id.logout:
//                navController.navigate(R.id.SecondFragment);
                logoutConfirmation();
                break;

        }
        return true;

    }

    private void logoutConfirmation() {

        Dialog logoutDialog = new Dialog(this.activity);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setContentView(R.layout.custom_common_popup);
        logoutDialog.setCanceledOnTouchOutside(true);
        logoutDialog.setCancelable(true);
        logoutDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        TextView txtViewTitle = logoutDialog.findViewById(R.id.txt_view_title);
        txtViewTitle.setText(getResources().getString(R.string.logout));

        TextView txtViewMsg = logoutDialog.findViewById(R.id.txt_view_msg);
        txtViewMsg.setText(getResources().getString(R.string.are_you_sure_logout));

        TextView txtViewOk = logoutDialog.findViewById(R.id.txt_view_ok);

        TextView txtViewCancel = logoutDialog.findViewById(R.id.txt_view_cancel);

        txtViewOk.setOnClickListener(view -> {

            if (logoutDialog != null && logoutDialog.isShowing()) {
                logoutDialog.dismiss();
            }

            preferenceUtil.setLogin(false);
            Intent intent = new Intent(activity, LogInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });


        txtViewCancel.setOnClickListener(view -> {
            if (logoutDialog != null && logoutDialog.isShowing()) {
                logoutDialog.dismiss();
            }
        });

        logoutDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_notification) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.under_development));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
