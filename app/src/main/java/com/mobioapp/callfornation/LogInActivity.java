package com.mobioapp.callfornation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.mobioapp.callfornation.controller.LoginController;
import com.mobioapp.callfornation.database.PreferenceUtil;
import com.mobioapp.callfornation.listener.LoginListener;
import com.mobioapp.callfornation.model.LoginInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mobioapp.callfornation.utils.Utils.isOnline;

public class LogInActivity extends AppCompatActivity implements LoginListener {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.etUserName)
    EditText editTextUserName;

    @BindView(R.id.etPassword)
    EditText editTextPassword;

    @BindView(R.id.layoutTextInputPasswordLayout)
    TextInputLayout textInputLayoutPassword;

    @BindView(R.id.btnLogin)
    Button buttonLogin;

    private Activity activity;

    private PreferenceUtil preferenceUtil;

    private LoginController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        initViews();
        checkIfUserLoggedIn();

    }

    private void checkIfUserLoggedIn() {

        if (preferenceUtil.isLoggedIn()) {
            goToDashboard();
        }

    }

    private void initViews() {
        activity = LogInActivity.this;
        preferenceUtil = new PreferenceUtil(activity);
    }

    @OnClick(R.id.linearLayoutSignUp)
    void goToRegistration() {
        startActivity(new Intent(activity, RegistrationActivity.class));
    }

    @OnClick(R.id.btnLogin)
    void submitLogin() {

        if (!isOnline(activity)) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
            return;
        }

        if (editTextUserName.getText().length() <= 0 || editTextPassword.getText().length() <= 0) {
            new MyCustomToast(getApplicationContext()).showToast(getResources().getString(R.string.provide_proper_login_credential));
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        buttonLogin.setEnabled(false);

        controller = new LoginController(this);
        controller.start(editTextUserName.getText().toString().trim(), editTextPassword.getText().toString().trim());
    }

    @Override
    public void loginCompleted(LoginInfo loginInfo) {
        progressBar.setVisibility(View.GONE);
        buttonLogin.setEnabled(true);

        if (loginInfo != null && "201".equals(loginInfo.getStatus())) {
            preferenceUtil.setLogin(true);
            preferenceUtil.updateLoginData(loginInfo);
            goToDashboard();
        } else if (loginInfo != null && "401".equals(loginInfo.getStatus())) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(loginInfo.getMsg());
        } else {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.there_is_a_problem));
        }
    }

    private void goToDashboard() {
        startActivity(new Intent(activity, MainActivity.class));
        finish();
    }

    @Override
    public void loginFailed(String message) {
        progressBar.setVisibility(View.GONE);
        buttonLogin.setEnabled(true);
        new MyCustomToast(getApplicationContext()).showToast(getResources().getString(R.string.failed_to_login));
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (controller != null)
            controller.removeListener();
    }
}