package com.mobioapp.callfornation;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.MediatorLiveData;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mobioapp.callfornation.controller.AddDistributionController;
import com.mobioapp.callfornation.controller.AddUpazillaListController;
import com.mobioapp.callfornation.customviews.imagePicker.ImageCompression;
import com.mobioapp.callfornation.customviews.imagePicker.ImageCompressionListener;
import com.mobioapp.callfornation.database.PreferenceUtil;
import com.mobioapp.callfornation.listener.AddDistributionListener;
import com.mobioapp.callfornation.listener.AddUpazillaListListener;
import com.mobioapp.callfornation.model.AddDistributionInfo;
import com.mobioapp.callfornation.model.ReliefDistribution;
import com.mobioapp.callfornation.model.Upazilla;
import com.mobioapp.callfornation.model.UpazillaInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.CurrentLocationFetcher;
import com.mobioapp.callfornation.utils.MyCustomToast;
import com.mobioapp.callfornation.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_CANCEL;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_DONE;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_NEW;
import static com.mobioapp.callfornation.utils.ConstantValues.RELIEF_DISTRIBUTION_PENDING;
import static com.mobioapp.callfornation.utils.ConstantValues.TAG_SELECTED_RELIEF_DISTRIBUTION;
import static com.mobioapp.callfornation.utils.ConstantValues.TAG_WHICH_OPTION_SELECTED;
import static com.mobioapp.callfornation.utils.Utils.isOnline;

public class ReliefDistributionAddActivity extends AppCompatActivity implements AddDistributionListener, AddUpazillaListListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.spinner_area)
    Spinner spinnerArea;

    @BindView(R.id.etAddress)
    EditText etAddress;

    @BindView(R.id.etReliefDetails)
    EditText editTextReliefDetails;

    @BindView(R.id.etNoOfDistributedFamily)
    EditText editTextNoOfDistributedFamily;

    @BindView(R.id.etNeedToCoverFamily)
    EditText editTextNeedToCover;

    @BindView(R.id.etNoOfDaysFamilySurvive)
    EditText editTextNoOfDaysFamilySurvive;

    @BindView(R.id.linearLayoutFamilyNeedsCovered)
    LinearLayout linearLayoutFamilyNeedsCovered;

    @BindView(R.id.layoutTextInputNoOfDistributedFamily)
    TextInputLayout layoutTextInputNoOfDistributedFamily;

    @BindView(R.id.btnSubmit)
    Button buttonSubmit;

    @BindView(R.id.btnCancel)
    Button buttonCancel;

    @BindView(R.id.rootLayout)
    ScrollView rootLayout;

    @BindView(R.id.frmInteractionPreventer)
    FrameLayout frmInteractionPreventer;

    @BindView(R.id.frameLayout1)
    FrameLayout frameLayoutDisPhoto1;

    @BindView(R.id.ivDistributionPhoto1)
    ImageView imageViewDisPhoto1;

    @BindView(R.id.imageViewPlusIconPhoto1)
    ImageView imageViewPlus1;

    @BindView(R.id.linearLayoutStatusDate)
    LinearLayout linearLayoutStatusDate;

    @BindView(R.id.linearLayoutDistributedPhoto)
    LinearLayout linearLayoutDistributedPhoto;

    @BindView(R.id.textViewDistributionDate)
    TextView textViewDate;

    private Activity activity;
    private AddDistributionController controller;
    private AddUpazillaListController controllerUpazilla;

    private ReliefDistribution reliefDistribution;

    private String whichOption = "";
    private String latitude = "";
    private String longitude = "";
    private PreferenceUtil preferenceUtil;
    private List<Upazilla> upazillaList = new ArrayList<>();
    private List<String> upazillaNameList = new ArrayList<>();
    CurrentLocationFetcher currentLocationFetcher = new CurrentLocationFetcher(this);
    private int API_COUNT = 2;

    private File filePhoto1 = null;
    private ImagePicker imagePicker;

    private static final int SELECT_IMAGE_1 = 121;

    private MediatorLiveData<Boolean> progressListener = new MediatorLiveData<Boolean>() {
        int completeCount = 0;

        @Override
        public void setValue(Boolean value) {
            if (value && progressBar.getVisibility() != View.VISIBLE)
                super.setValue(true);
            else if (!value) {
                completeCount++;
                if (completeCount >= API_COUNT) {
                    completeCount = 0;
                    super.setValue(false);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_distribution);
        ButterKnife.bind(this);

        getIntentData();
        setToolbar();
        initViews();
    }

    private void setToolbar() {

        if (toolbar != null)
            toolbar.setTitle(whichOption.equals(RELIEF_DISTRIBUTION_PENDING) ? getResources().getString(R.string.edit_distribution) : getResources().getString(R.string.new_distribution));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.getExtras().getString(TAG_WHICH_OPTION_SELECTED) != null) {
                whichOption = intent.getExtras().getString(TAG_WHICH_OPTION_SELECTED);
                if (intent.getExtras().getSerializable(TAG_SELECTED_RELIEF_DISTRIBUTION) != null) {
                    reliefDistribution = (ReliefDistribution) intent.getSerializableExtra(TAG_SELECTED_RELIEF_DISTRIBUTION);
                }
            }
        }

    }

    private void initViews() {

        activity = ReliefDistributionAddActivity.this;
        preferenceUtil = new PreferenceUtil(activity);

        populateViewsAccordingToOptions();
        getUpazillaList();
        configImagePicker();

        progressListener.observe(this, shouldShow -> {
            int visibility = shouldShow ? View.VISIBLE : View.GONE;
            progressBar.setVisibility(visibility);
            frmInteractionPreventer.setVisibility(visibility);
            if (shouldShow) {
                frmInteractionPreventer.bringToFront();
            }
        });

        if (!whichOption.equals(RELIEF_DISTRIBUTION_PENDING)) {
            initLocation();
            progressListener.postValue(true);
        }

    }

    private void populateViewsAccordingToOptions() {

        if (whichOption.equals(RELIEF_DISTRIBUTION_NEW)) {
            textViewDate.setText(Utils.getCurrentDateToDisplay());
            linearLayoutStatusDate.setVisibility(View.GONE);
            linearLayoutDistributedPhoto.setVisibility(View.GONE);
        } else if (whichOption.equals(RELIEF_DISTRIBUTION_PENDING)) {
            buttonCancel.setVisibility(View.VISIBLE);
            if (reliefDistribution != null) {
                spinnerArea.setSelection(getUpazillaPositionFromName(reliefDistribution.getUpazila_name().split(">")[1].trim()));
                textViewDate.setText(Utils.convertDateToHumanReadableFromServer(reliefDistribution.getDate_of_distribution()));
                etAddress.setText(reliefDistribution.getAddress());
                editTextReliefDetails.setText(reliefDistribution.getReleife_items());
                editTextNoOfDistributedFamily.setText(reliefDistribution.getNo_of_family());
                editTextNoOfDaysFamilySurvive.setText(reliefDistribution.getSurvival_day());
                editTextNeedToCover.setText(reliefDistribution.getIs_needed());
                buttonSubmit.setText("Distributed");
            }
        }

    }

    private void initLocation() {

        Dexter.withContext(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    {
                        boolean successful = currentLocationFetcher.fetchOnce();
                        Log.d("LocationFetch", successful + "");
                    }
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }
        }).check();

        currentLocationFetcher.getCurrentAddressLiveData().observe(this, address -> {
            if (address != null)
                etAddress.setText(address);
            progressListener.postValue(false);
        });

        currentLocationFetcher.getLocationLiveData().observe(this, latLng -> {
            latitude = String.valueOf(latLng.latitude);
            longitude = String.valueOf(latLng.longitude);
        });


    }

    private void configImagePicker() {
        imagePicker = ImagePicker.create(activity)
                .theme(R.style.ImagePickerTheme)
                .single()
                .returnMode(ReturnMode.ALL) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(true) // set folder mode (false by default)
                .toolbarArrowColor(Color.WHITE) // set toolbar arrow up color
                .toolbarFolderTitle("Images Folders") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .toolbarDoneButtonText("DONE"); // done button text
    }

    private void processSelectedImage(String filePath, int image_type_code) {
        Bitmap selectedImage = null;
        try {
            selectedImage = BitmapFactory.decodeFile(filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (image_type_code == SELECT_IMAGE_1) {

//            GlideApp.with(ProfileVerifyActivity.this)
//                    .clear(ivFrontPhoto); // preventing loading again failed request

            if (selectedImage != null) {
                imageViewDisPhoto1.setImageBitmap(selectedImage);
                imageViewPlus1.setVisibility(View.GONE);
            }

            new ImageCompression(activity,
                    filePath, new ImageCompressionListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onCompressed(String filePath) {
                    filePhoto1 = new File(filePath);
                }
            }).execute();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("Permission", "");
    }

    private void getUpazillaList() {
        controllerUpazilla = new AddUpazillaListController(this);
        controllerUpazilla.start();
    }

    @OnClick(R.id.btnCancel)
    void submitCancel() {
        cancelConfirmation();
    }

    private void cancelDistribution() {

        if (!isOnline(activity)) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        controller = new AddDistributionController(this);
        controller.update(preferenceUtil.retrieveLoginData().getUser_id(),
                getRespectiveUpazillaIdFromName(spinnerArea.getSelectedItem().toString().trim()),
                editTextNoOfDistributedFamily.getText().toString().trim(),
                editTextReliefDetails.getText().toString().trim(),
                editTextNoOfDaysFamilySurvive.getText().toString().trim(),
                editTextNeedToCover.getText().toString().trim(),
                editTextNeedToCover.getText().toString().trim() + " family needs to be covered.",
                Utils.getCurrentDateForServer(),
                etAddress.getText().toString().trim(),
                latitude,
                longitude,
                RELIEF_DISTRIBUTION_CANCEL,
                reliefDistribution.getId(),
                filePhoto1);

    }

    @OnClick(R.id.btnSubmit)
    void submitDistribution() {

        if (!isOnline(activity)) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.no_internet_message));
            return;
        }

        if (spinnerArea.getSelectedItemPosition() == 0) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast("Please select your area");
            return;
        }

        if (etAddress.getText().toString().trim().length() <= 0
                || editTextReliefDetails.getText().toString().trim().length() <= 0
                || editTextNoOfDistributedFamily.getText().toString().trim().length() <= 0
                || editTextNoOfDaysFamilySurvive.getText().toString().trim().length() <= 0) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast("Please provide all information.");
            return;
        }

        if (whichOption.equals(RELIEF_DISTRIBUTION_PENDING)) {
            if (filePhoto1 == null) {
                new MyCustomToast(CFNApplication.getAppContext()).showToast("Please upload at least one photo");
                return;
            }
        }

        progressBar.setVisibility(View.VISIBLE);

        controller = new AddDistributionController(this);
        if (whichOption.equals(RELIEF_DISTRIBUTION_NEW)) {
            controller.start(preferenceUtil.retrieveLoginData().getUser_id(),
                    getRespectiveUpazillaIdFromName(spinnerArea.getSelectedItem().toString().trim()),
                    editTextNoOfDistributedFamily.getText().toString().trim(),
                    editTextReliefDetails.getText().toString().trim(),
                    editTextNoOfDaysFamilySurvive.getText().toString().trim(),
                    editTextNeedToCover.getText().toString().trim(),
                    editTextNeedToCover.getText().toString().trim() + " family needs to be covered.",
                    Utils.getCurrentDateForServer(),
                    etAddress.getText().toString().trim(),
                    latitude,
                    longitude,
                    "");
        } else if (whichOption.equals(RELIEF_DISTRIBUTION_PENDING)) {

            controller.update(preferenceUtil.retrieveLoginData().getUser_id(),
                    getRespectiveUpazillaIdFromName(spinnerArea.getSelectedItem().toString().trim()),
                    editTextNoOfDistributedFamily.getText().toString().trim(),
                    editTextReliefDetails.getText().toString().trim(),
                    editTextNoOfDaysFamilySurvive.getText().toString().trim(),
                    editTextNeedToCover.getText().toString().trim(),
                    editTextNeedToCover.getText().toString().trim() + " family needs to be covered.",
                    Utils.getCurrentDateForServer(),
                    etAddress.getText().toString().trim(),
                    latitude,
                    longitude,
                    RELIEF_DISTRIBUTION_DONE,
                    reliefDistribution.getId(),
                    filePhoto1);
        }

    }

    @Override
    public void addDistributionCompleted(AddDistributionInfo addDistributionInfo) {
        progressListener.postValue(false);
        progressBar.setVisibility(View.GONE);
        if (addDistributionInfo != null) {
            if (addDistributionInfo.status.equals("201")) {
                new MyCustomToast(CFNApplication.getAppContext()).showToast("Data updated!");
                finish();
            } else {
                new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.there_is_a_problem));
            }
        } else {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(getResources().getString(R.string.there_is_a_problem));
        }
    }

    @Override
    public void addDistributionFailed(String message) {
        progressBar.setVisibility(View.GONE);
        progressListener.postValue(false);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (controller != null)
            controller.removeListener();

        if (controllerUpazilla != null)
            controllerUpazilla.removeListener();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void addUpazillaListCompleted(UpazillaInfo upazillaInfo) {

        progressListener.postValue(false);

        if (upazillaInfo != null) {
            this.upazillaList = upazillaInfo.upazila_list;

            if (upazillaList != null && upazillaList.size() > 0) {
                upazillaNameList = new ArrayList<>();

                for (int i = 0; i < upazillaList.size(); i++) {
                    upazillaNameList.add(upazillaList.get(i).upazila_name);
                }

                loadSpinnerData(upazillaNameList);
            }
        }

    }

    @Override
    public void addUpazillaListFailed(String message) {
        progressListener.postValue(false);
        new MyCustomToast(CFNApplication.getAppContext()).showToast("Data syncing failed!");
    }

    private void loadSpinnerData(List<String> upazillaNameList) {

        Collections.sort(upazillaNameList, String::compareToIgnoreCase);

        upazillaNameList.add(0, "Distribution Area");

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        upazillaNameList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerArea.setAdapter(spinnerArrayAdapter);

        if (whichOption.equals(RELIEF_DISTRIBUTION_PENDING)) {
            if (reliefDistribution != null) {
                spinnerArea.setSelection(getUpazillaPositionFromName(reliefDistribution.getUpazila_name().split(">")[1].trim()));
            }
        }

    }

    private String getRespectiveUpazillaIdFromName(String upazillaName) {
        for (int i = 0; i < upazillaList.size(); i++) {
            if (upazillaList.get(i).upazila_name.equalsIgnoreCase(upazillaName)) {
                return upazillaList.get(i).id;
            }
        }
        return "0";
    }

    private int getUpazillaPositionFromName(String upazillaName) {
        if (upazillaNameList == null || upazillaNameList.size() == 0)
            return 0;
        for (int i = 0; i < upazillaNameList.size(); i++) {
            if (upazillaNameList.get(i).equalsIgnoreCase(upazillaName)) {
                return i;
            }
        }
        return 0;
    }

    @OnClick(R.id.frameLayout1)
    void choosePhoto() {
        Dexter.withContext(this)
                .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            imagePicker.start(SELECT_IMAGE_1);

                        } else {
                            showSnackBar();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                                   PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showSnackBar() {
        Snackbar.make(rootLayout, getString(R.string.permission_storage_messge), Snackbar.LENGTH_LONG)
                .setAction("SETTINGS", view -> Utils.redirectToAppSettings(activity))
                .setActionTextColor(getResources().getColor(android.R.color.holo_orange_light))
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == SELECT_IMAGE_1) {
            Image image = ImagePicker.getFirstImageOrNull(data);
            Log.d("image %s code %d", image.toString() + "____" + requestCode);
            processSelectedImage(image.getPath(), SELECT_IMAGE_1);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void cancelConfirmation() {

        Dialog cancelDialog = new Dialog(this.activity);
        cancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cancelDialog.setContentView(R.layout.custom_common_popup);
        cancelDialog.setCanceledOnTouchOutside(true);
        cancelDialog.setCancelable(true);
        cancelDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        TextView txtViewTitle = cancelDialog.findViewById(R.id.txt_view_title);
        txtViewTitle.setText("Cancel Distribution");

        TextView txtViewMsg = cancelDialog.findViewById(R.id.txt_view_msg);
        txtViewMsg.setText(getResources().getString(R.string.are_you_sure_want_to_cancel));

        TextView txtViewOk = cancelDialog.findViewById(R.id.txt_view_ok);
        txtViewOk.setText(getResources().getString(R.string.ok));

        TextView txtViewCancel = cancelDialog.findViewById(R.id.txt_view_cancel);
        txtViewCancel.setText(getResources().getString(R.string.no));

        txtViewOk.setOnClickListener(view -> {

            if (cancelDialog != null && cancelDialog.isShowing()) {
                cancelDialog.dismiss();
            }

            cancelDistribution();
        });


        txtViewCancel.setOnClickListener(view -> {
            if (cancelDialog != null && cancelDialog.isShowing()) {
                cancelDialog.dismiss();
            }
        });

        cancelDialog.show();
    }
}