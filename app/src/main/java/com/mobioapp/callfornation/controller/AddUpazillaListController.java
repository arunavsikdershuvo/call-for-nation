package com.mobioapp.callfornation.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.configuration.AppConfig;
import com.mobioapp.callfornation.controller.common.NetworkConnectionInterceptor;
import com.mobioapp.callfornation.controller.common.NoConnectivityException;
import com.mobioapp.callfornation.listener.AddUpazillaApi;
import com.mobioapp.callfornation.listener.AddUpazillaListListener;
import com.mobioapp.callfornation.model.UpazillaInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddUpazillaListController implements Callback<UpazillaInfo> {
    private AddUpazillaListListener listener = null;

    public AddUpazillaListController(AddUpazillaListListener listener) {
        this.listener = listener;
    }

    public void start() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        AddUpazillaApi addGatewayApi = retrofit.create(AddUpazillaApi.class);
        Call<UpazillaInfo> call = addGatewayApi.addDistribution();
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<UpazillaInfo> call, Response<UpazillaInfo> response) {
        if (response.isSuccessful()) {
            UpazillaInfo upazillaInfo = response.body();
            if (listener != null) listener.addUpazillaListCompleted(upazillaInfo);
        } else {
            if (listener != null) listener.addUpazillaListFailed(response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<UpazillaInfo> call, Throwable throwable) {
        if (listener != null && throwable != null && throwable.getMessage() != null) {
            listener.addUpazillaListFailed(throwable.getMessage());
        }

        if (throwable instanceof NoConnectivityException) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(CFNApplication.getAppContext().getResources().getString(R.string.no_internet_message));
        }
    }

    public void removeListener() {
        this.listener = null;
    }
}