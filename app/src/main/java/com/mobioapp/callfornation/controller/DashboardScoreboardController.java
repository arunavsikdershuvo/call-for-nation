package com.mobioapp.callfornation.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.configuration.AppConfig;
import com.mobioapp.callfornation.controller.common.NetworkConnectionInterceptor;
import com.mobioapp.callfornation.controller.common.NoConnectivityException;
import com.mobioapp.callfornation.listener.DashboardScoreApi;
import com.mobioapp.callfornation.listener.DashboardScoreboardListener;
import com.mobioapp.callfornation.model.DashboardScoreboardInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DashboardScoreboardController implements Callback<DashboardScoreboardInfo> {
    private DashboardScoreboardListener listener = null;

    public DashboardScoreboardController(DashboardScoreboardListener listener) {
        this.listener = listener;
    }

    public void start() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        DashboardScoreApi addGatewayApi = retrofit.create(DashboardScoreApi.class);
        Call<DashboardScoreboardInfo> call = addGatewayApi.loadDashboardScore();
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<DashboardScoreboardInfo> call, Response<DashboardScoreboardInfo> response) {
        if (response.isSuccessful()) {
            DashboardScoreboardInfo dashboardScoreboardInfo = response.body();
            if (listener != null) listener.loadScoreboardCompleted(dashboardScoreboardInfo);
        } else {
            if (listener != null) listener.loadScoreboardFailed(response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<DashboardScoreboardInfo> call, Throwable throwable) {
        if (listener != null && throwable != null && throwable.getMessage() != null) {
            listener.loadScoreboardFailed(throwable.getMessage());
        }

        if (throwable instanceof NoConnectivityException) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(CFNApplication.getAppContext().getResources().getString(R.string.no_internet_message));
        }
    }

    public void removeListener() {
        this.listener = null;
    }
}