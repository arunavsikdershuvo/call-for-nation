package com.mobioapp.callfornation.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.configuration.AppConfig;
import com.mobioapp.callfornation.controller.common.NetworkConnectionInterceptor;
import com.mobioapp.callfornation.controller.common.NoConnectivityException;
import com.mobioapp.callfornation.listener.AddReliefRequestApi;
import com.mobioapp.callfornation.listener.AddReliefRequestListener;
import com.mobioapp.callfornation.model.AddReliefRequestInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddReliefRequestController implements Callback<AddReliefRequestInfo> {
    private AddReliefRequestListener listener = null;

    public AddReliefRequestController(AddReliefRequestListener listener) {
        this.listener = listener;
    }

    public void start(String user_id, String upazilla_id, String type, String name, String address,
                      String no_of_family, String relief_items, String no_of_survival_day,
                      String details, String nid_no) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        AddReliefRequestApi addGatewayApi = retrofit.create(AddReliefRequestApi.class);
        Call<AddReliefRequestInfo> call = addGatewayApi.addReliefRequest(user_id,
                 upazilla_id, type, name, address, no_of_family, relief_items,
                no_of_survival_day, details, nid_no);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<AddReliefRequestInfo> call, Response<AddReliefRequestInfo> response) {
        if (response.isSuccessful()) {
            AddReliefRequestInfo addReliefRequestInfo = response.body();
            if (listener != null) listener.addReliefRequestCompleted(addReliefRequestInfo);
        } else {
            if (listener != null) listener.addReliefRequestFailed(response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<AddReliefRequestInfo> call, Throwable throwable) {
        if (listener != null && throwable != null && throwable.getMessage() != null) {
            listener.addReliefRequestFailed(throwable.getMessage());
        }

        if (throwable instanceof NoConnectivityException) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(CFNApplication.getAppContext().getResources().getString(R.string.no_internet_message));
        }
    }

    public void removeListener() {
        this.listener = null;
    }
}
