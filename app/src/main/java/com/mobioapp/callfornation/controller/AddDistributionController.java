package com.mobioapp.callfornation.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.configuration.AppConfig;
import com.mobioapp.callfornation.controller.common.NetworkConnectionInterceptor;
import com.mobioapp.callfornation.controller.common.NoConnectivityException;
import com.mobioapp.callfornation.listener.AddDistributionApi;
import com.mobioapp.callfornation.listener.AddDistributionListener;
import com.mobioapp.callfornation.model.AddDistributionInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddDistributionController implements Callback<AddDistributionInfo> {
    private AddDistributionListener listener = null;

    public AddDistributionController(AddDistributionListener listener) {
        this.listener = listener;
    }

    public void start(String user_id, String upazilla_id, String no_of_family_served,
                      String relief_items, String survival_day, String is_needed, String is_needed_details,
                      String date, String address, String latitude, String longitude, String photo) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        AddDistributionApi addGatewayApi = retrofit.create(AddDistributionApi.class);
        Call<AddDistributionInfo> call = addGatewayApi.addDistribution(user_id, upazilla_id,
                (no_of_family_served == null || no_of_family_served.isEmpty()) ? 0 : Integer.parseInt(no_of_family_served), relief_items, survival_day,
                (is_needed == null || is_needed.isEmpty()) ? 0 : Integer.parseInt(is_needed), is_needed_details, date, address, latitude, longitude,
                photo);
        call.enqueue(this);
    }

    public void update(String user_id, String upazilla_id, String no_of_family_served,
                       String relief_items, String survival_day, String is_needed, String is_needed_details,
                       String date, String address, String latitude, String longitude, String status,
                       String id, File photo) {

        RequestBody filePhoto1;
        MultipartBody.Part bodyPhoto1 = null;

        if (photo != null) {
            filePhoto1 = RequestBody.create(photo, MediaType.parse("image/*"));
            bodyPhoto1 = MultipartBody.Part.createFormData("photo", photo.getName(), filePhoto1);
        }

        RequestBody user_id_body = RequestBody.create(user_id, MediaType.parse("multipart/form-data"));
        RequestBody upazilla_id_body = RequestBody.create(upazilla_id, MediaType.parse("multipart/form-data"));
        RequestBody no_of_family_served_body = RequestBody.create(no_of_family_served, MediaType.parse("multipart/form-data"));
        RequestBody relief_items_body = RequestBody.create(relief_items, MediaType.parse("multipart/form-data"));
        RequestBody survival_day_body = RequestBody.create(survival_day, MediaType.parse("multipart/form-data"));
        RequestBody is_needed_body = RequestBody.create(is_needed, MediaType.parse("multipart/form-data"));
        RequestBody is_needed_details_body = RequestBody.create(is_needed_details, MediaType.parse("multipart/form-data"));
        RequestBody date_body = RequestBody.create(date, MediaType.parse("multipart/form-data"));
        RequestBody address_body = RequestBody.create(address, MediaType.parse("multipart/form-data"));
        RequestBody latitude_body = RequestBody.create(latitude, MediaType.parse("multipart/form-data"));
        RequestBody longitude_body = RequestBody.create(longitude, MediaType.parse("multipart/form-data"));
        RequestBody status_body = RequestBody.create(status, MediaType.parse("multipart/form-data"));
        RequestBody id_body = RequestBody.create(id, MediaType.parse("multipart/form-data"));

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        AddDistributionApi addDistributionApi = retrofit.create(AddDistributionApi.class);
        Call<AddDistributionInfo> call = addDistributionApi.updateDistribution(user_id_body, upazilla_id_body,
                no_of_family_served_body, relief_items_body, survival_day_body,
                is_needed_body, is_needed_details_body, date_body, address_body, latitude_body, longitude_body,
                status_body, id_body, bodyPhoto1);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<AddDistributionInfo> call, Response<AddDistributionInfo> response) {
        if (response.isSuccessful()) {
            AddDistributionInfo addDistributionInfo = response.body();
            if (listener != null) listener.addDistributionCompleted(addDistributionInfo);
        } else {
            if (listener != null) listener.addDistributionFailed(response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<AddDistributionInfo> call, Throwable throwable) {
        if (listener != null && throwable != null && throwable.getMessage() != null) {
            listener.addDistributionFailed(throwable.getMessage());
        }

        if (throwable instanceof NoConnectivityException) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(CFNApplication.getAppContext().getResources().getString(R.string.no_internet_message));
        }
    }

    public void removeListener() {
        this.listener = null;
    }
}