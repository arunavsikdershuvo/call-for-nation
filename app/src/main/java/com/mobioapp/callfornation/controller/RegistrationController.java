package com.mobioapp.callfornation.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.configuration.AppConfig;
import com.mobioapp.callfornation.controller.common.NetworkConnectionInterceptor;
import com.mobioapp.callfornation.controller.common.NoConnectivityException;
import com.mobioapp.callfornation.listener.RegistrationApi;
import com.mobioapp.callfornation.listener.RegistrationListener;
import com.mobioapp.callfornation.model.RegistrationInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationController implements Callback<RegistrationInfo> {
    private RegistrationListener listener = null;

    public RegistrationController(RegistrationListener listener) {
        this.listener = listener;
    }

    public void start(String fullName, String userName, String password, String phoneNumber,
                      String email, String address, String organisationType) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        RegistrationApi registrationApi = retrofit.create(RegistrationApi.class);
        Call<RegistrationInfo> call = registrationApi.register(fullName, userName, password,
                phoneNumber, email, address, organisationType);
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<RegistrationInfo> call, Response<RegistrationInfo> response) {
        if (response.isSuccessful()) {
            RegistrationInfo registrationInfo = response.body();
            if (listener != null) listener.registrationCompleted(registrationInfo);
        } else {
            if (listener != null) listener.registrationFailed(response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<RegistrationInfo> call, Throwable throwable) {
        if (listener != null && throwable != null && throwable.getMessage() != null) {
            listener.registrationFailed(throwable.getMessage());
        }

        if (throwable instanceof NoConnectivityException) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(CFNApplication.getAppContext().getResources().getString(R.string.no_internet_message));
        }
    }

    public void removeListener() {
        this.listener = null;
    }
}