package com.mobioapp.callfornation.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.configuration.AppConfig;
import com.mobioapp.callfornation.controller.common.NetworkConnectionInterceptor;
import com.mobioapp.callfornation.controller.common.NoConnectivityException;
import com.mobioapp.callfornation.listener.ReliefDistributionListApi;
import com.mobioapp.callfornation.listener.ReliefDistributionListListener;
import com.mobioapp.callfornation.model.ReliefDistributionListInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReliefDistributionListController implements Callback<ReliefDistributionListInfo> {
    private ReliefDistributionListListener listener = null;

    public ReliefDistributionListController(ReliefDistributionListListener listener) {
        this.listener = listener;
    }

    public void start(String user_id) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        ReliefDistributionListApi reliefDistributionListApi = retrofit.create(ReliefDistributionListApi.class);
        Call<ReliefDistributionListInfo> call = reliefDistributionListApi.getUserWiseReliefDistributionList(user_id);
        call.enqueue(this);
    }

    public void startStatus(String status) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        ReliefDistributionListApi reliefDistributionListApi = retrofit.create(ReliefDistributionListApi.class);
        Call<ReliefDistributionListInfo> call = reliefDistributionListApi.getStatusWiseReliefDistributionList(status);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<ReliefDistributionListInfo> call, Response<ReliefDistributionListInfo> response) {
        if (response.isSuccessful()) {
            ReliefDistributionListInfo reliefDistributionListInfo = response.body();
            if (listener != null) listener.distributionInfoCompleted(reliefDistributionListInfo);
        } else {
            if (listener != null) listener.distributionInfoFailed(response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<ReliefDistributionListInfo> call, Throwable throwable) {
        if (listener != null && throwable != null && throwable.getMessage() != null) {
            listener.distributionInfoFailed(throwable.getMessage());
        }

        if (throwable instanceof NoConnectivityException) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(CFNApplication.getAppContext().getResources().getString(R.string.no_internet_message));
        }
    }

    public void removeListener() {
        this.listener = null;
    }
}
