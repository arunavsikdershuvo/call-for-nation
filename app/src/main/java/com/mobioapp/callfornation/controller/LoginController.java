package com.mobioapp.callfornation.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobioapp.callfornation.R;
import com.mobioapp.callfornation.configuration.AppConfig;
import com.mobioapp.callfornation.controller.common.NetworkConnectionInterceptor;
import com.mobioapp.callfornation.controller.common.NoConnectivityException;
import com.mobioapp.callfornation.listener.LoginApi;
import com.mobioapp.callfornation.listener.LoginListener;
import com.mobioapp.callfornation.model.LoginInfo;
import com.mobioapp.callfornation.utils.CFNApplication;
import com.mobioapp.callfornation.utils.MyCustomToast;

import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginController implements Callback<LoginInfo> {
    private LoginListener listener = null;

    public LoginController(LoginListener listener) {
        this.listener = listener;
    }

    public void start(String userName, String password) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new NetworkConnectionInterceptor(CFNApplication.getAppContext()))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        LoginApi loginApi = retrofit.create(LoginApi.class);
        Call<LoginInfo> call = loginApi.login(userName, password);
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<LoginInfo> call, Response<LoginInfo> response) {
        if (response.isSuccessful()) {
            LoginInfo loginInfo = response.body();
            if (listener != null) listener.loginCompleted(loginInfo);
        } else {
            if (listener != null) listener.loginFailed(response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<LoginInfo> call, Throwable throwable) {
        if (listener != null && throwable != null && throwable.getMessage() != null) {
            listener.loginFailed(throwable.getMessage());
        }

        if (throwable instanceof NoConnectivityException) {
            new MyCustomToast(CFNApplication.getAppContext()).showToast(CFNApplication.getAppContext().getResources().getString(R.string.no_internet_message));
        }
    }

    public void removeListener() {
        this.listener = null;
    }
}