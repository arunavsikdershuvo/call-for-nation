package com.mobioapp.callfornation.controller.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Shubhobrata Roy
 * on 9/26/19
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SuccessResponse<T> {
    ResponseMetadata responseMetadata;
    T responseData;

    public boolean isCompletelySuccessful() {
        return responseMetadata.isSuccessful() && responseData != null;
    }
}
