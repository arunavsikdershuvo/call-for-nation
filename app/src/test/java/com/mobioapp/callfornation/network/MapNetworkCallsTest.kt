package com.mobioapp.callfornation.network

import com.mobioapp.callfornation.map.network.MapNetworkCalls
import org.junit.Assert.assertNotNull
import org.junit.Test

class MapNetworkCallsTest {
    val mapNetworkCalls = MapNetworkCalls()
    @Test
    fun getWelfareData() {

        val welfareData = mapNetworkCalls.getWelfareData()?.body()
        assertNotNull(welfareData)

        print(welfareData)
    }

    @Test
    fun testUpozillaById() {
        val body = mapNetworkCalls.getDistributionByUpozilla("1")?.body()
        assertNotNull(body)
        print(body)
    }
}